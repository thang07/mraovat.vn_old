<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>mRaovat Push Notification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="index, follow" name="robots"/>

    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="http://mraovat.vn/images/img-mobile.png" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="BÁN LIỀN TAY MUA TRONG NGÀY" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />

    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
    {{--<link href="{{asset('/css/bootstrap-responsive.css')}}" rel="stylesheet">--}}
    <link href="{{asset('/css/normalize.css')}}" rel="stylesheet">
    <style>
        .container {
            width: 600px;
        }

        .table-borderless td {
            border: none !important;
        }
    </style>
</head>
<body>
<div class="container">
    hello! {{$user->user_title}}
    @if(Session::has('action') && Session::get('action'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
            <span>Gởi thành công</span>
        </div>
    @endif
    <div class="header">
        <h2>mRaovat Push Notification Tool</h2>
    </div>
    <div class="table-responsive">
        <form method="post" name="f" class="">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <table class="table table-borderless">
                <tr>
                    <td>Nội dung</td>
                    <td>
                        <textarea name="message" class="form-control" rows="3" placeholder="Nội dung push" required
                                  autofocus></textarea>
                    </td>
                </tr>
                <tr>
                    <td>ID danh mục</td>
                    <td>
                        <select class="form-control" name="forum_id">
                            <option value="0" selected>Chọn danh mục (optional)</option>
                            @foreach($categories as $cat)
                                <option value="{{$cat->forum_id}}">{{$cat->forum_title}}</option>
                            @endforeach
                        </select>
                        {{--<input name="forum_id" type="text" class="form-control" value=""--}}
                               {{--placeholder="ID Forum (optional)">--}}
                    </td>
                </tr>
                <tr>

                    <td>ID bài viết</td>
                    <td>
                        <input name="thread_id" type="text" class="form-control" value=""
                               placeholder="ID bài viết (optional)">
                    </td>
                </tr>
                {{--<tr>--}}
                {{--<td>Hệ điều hành</td>--}}
                {{--<td>--}}
                {{--<select class="form-control" name="pushTo">--}}
                {{--<option value="all">Tất cả({{$count}})</option>--}}
                {{--<option value="ios">iOS({{$countIOS}})</option>--}}
                {{--<option value="and">Android({{$countAnd}})</option>--}}
                {{--<option value="wp">WindowsPhone({{$countWP}})</option>--}}
                {{--</select>--}}
                {{--</td>--}}
                {{--</tr>--}}
                <tr>
                    <td></td>
                    <td>
                        <button type="submit" onclick="confirm('Xác nhận!\n Sẽ không thể dừng lại nếu như đã chấp nhận!')" class="btn btn-danger">Push</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>