@extends('layouts.home')
@section('title')
    mRaovat - Về chúng tôi
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="http://mraovat.vn/images/logo%202.png" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="BÁN LIỀN TAY MUA TRONG NGÀY" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />

@stop
@section('content')
    <div class="container">
        <div class="row section">
            <div class="col-md-12">
                <br/>

                <div class="text-center">
                    <img src="{{URL::to('/images/logo 2.png')}}">
                </div>
                <br/>

                <div class="text-justify">
                    <p>mRaovat được phát triển bởi <a href="#">CÔNG TY CỔ PHẦN CÔNG NGHỆ THANH VÂN</a> - nơi tập hợp
                        tác chuyên gia hàng đầu
                        trong lĩnh vực phát triển ứng dụng di động và thương mại điện tử tại Việt Nam.</p>

                    <p>
                        mRaovat là ứng dụng di động tốt nhất hiện nay hỗ trợ việc mua – bán dành cho di động.
                        mRaovat tập trung tối ưu hóa các tính năng có sẵn của thiết bị di động (điện thoại thông
                        minh, máy tính bảng) để việc bán hàng, mua hàng trở nên nhanh chóng và thuận tiện.</p>
                    Website mRaovat.vn được xây dựng nhằm hỗ trợ và mở rộng kênh giới thiệu hàng hóa, sản phẩm đến
                    những người dùng không có thiết bị di động.
                    <p>
                        Tại đây, mọi thông tin về hàng hóa và gian hàng của người bán sẽ được cập nhật từ các tin
                        đăng trên ứng dụng di động.</p>

                    <p>
                        Thông tin chi tiết xin liên hệ: <a href="mailto:contact@mRaovat.vn">contact@mRaovat.vn</a>
                    </p>
                </div>


            </div>
        </div>
    </div>
@stop

