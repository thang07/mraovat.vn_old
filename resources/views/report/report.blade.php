@extends('layouts.report')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
@stop

@section('content')
    <div class="container">
        <div class="row reports">
            <div class="col-sm-3">
                <label>Từ:</label>
                <input type="text" class="form-control" id="from"/>
            </div>
            <div class="col-sm-3">
                <label>Đến:</label>
                <input type="text" class="form-control" id="to"/>
            </div>
            <div class="col-sm-3">
                <label></label>
                <a href="#" id="filter" class="btn btn-primary btn-xong">Xong</a>
            </div>
            <div class="col-sm-3 text-right">
                <label><input type="radio" name="optradio" checked id="all"> Tất cả</label>
                <label><input type="radio" name="optradio" id="mobile"> Mobile</label>
            </div>
        </div>
        <div class="demo-container ">
            <div id="placeholder" class="demo-placeholder"></div>
        </div>
    </div>
@stop
@section('footer')
    <link href="{{asset('js/datepicker/css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('js/datepicker/less/datepicker.less')}}" rel="stylesheet">
    <script src="{{asset('js/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('js/flot/jquery.flot.time.js')}}"></script>
    <script type="text/javascript">

        $('#from').val('{{$start}}');
        $('#to').val('{{$end}}');

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);


        var from = $('#from').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function (date) {
                // return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > to.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                to.setValue(newDate);
            }
            from.hide();
            $('#to')[0].focus();
        }).data('datepicker');

        var to = $('#to').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function (date) {
                return date.valueOf() <= from.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            to.hide();
        }).data('datepicker');

        $(function () {
            var thread = '{!!$thread!!}';
            var threads = JSON.parse(thread);
            var user = '{!!$user!!}';
            var users = JSON.parse(user);
            var tick = '{!!$tick!!}';
            var ticks = JSON.parse(tick);
            var type = '{{$type}}';
            var option = "all";
            if (ticks.length == 0) {
                ticks = null;
            }
            doPlot(users, threads)

            $("#all").click(function () {
                option = "all"
                getData()
            });

            $("#mobile").click(function () {
                option = "app"
                getData()
            });

            $("#filter").click(function () {
                getData()
            });

            function getData() {
                var start = $("#from").val();
                var end = $("#to").val();
                $.blockUI({
                    message: 'Đang tải...',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '5px',
                        '-moz-border-radius': '5px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/report-data')}}",
                    data: {'start': start, 'end': end, 'type': type, 'option': option},
                    cache: false,
                    success: function (rs) {
                        var json = JSON.parse(rs);
                        var jsonTick = json.alias;
                        if (jsonTick.length == 0) {
                            ticks = null;
                        } else {
                            ticks = jsonTick;
                        }
                        doPlot(json.user, json.thread);
                        $.unblockUI()
                    },
                    error: function (xhr, status, error) {
                        //alert(xhr.responseText);
                    }
                });
            }

            function doPlot(users, threads) {

                $.plot("#placeholder", [
                    {
                        data: users, label: "User"
                    },
                    {
                        data: threads, label: "Thread"
                    }
                ], {
                    series: {
                        lines: {
                            show: true
                        },
                        points: {
                            show: true
                        }
                    },
                    xaxes: [
                        {
                            mode: "time",
                            timeformat: "%d/%m/%Y",
                            ticks: ticks
                        }
                    ],
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    yaxis: {
                        tickFormatter: function numberWithCommas(x) {
                            return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
                        }
                    }
                });
            }


            $("<div id='tooltip'></div>").css({
                position: "absolute",
                display: "none",
                border: "1px solid #fdd",
                padding: "2px",
                "background-color": "#000000",
                color: "#ffffff",
                opacity: 0.80
            }).appendTo("body");

            $("#placeholder").bind("plothover", function (event, pos, item) {
                if (item) {
                    var x = item.datapoint[0],
                            y = item.datapoint[1];

                    $("#tooltip").html(checkType(x, y, item))
                            .css({top: item.pageY + 5, left: item.pageX + 5})
                            .fadeIn(200);
                } else {
                    $("#tooltip").hide();
                }
            });

            function checkType(x, y, item) {
                if (ticks == null) {
                    return $.plot.formatDate(new Date(x), '%d/%m/%Y') + ", " + item.series.label + ": " + numberWithCommas(y);
                } else {
                    return ticks[x][1] + ", " + item.series.label + ": " + numberWithCommas(y)
                }
            }

            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
            }
        });
    </script>
@stop