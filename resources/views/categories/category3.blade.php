@extends('layouts.home')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn" />
    <meta name="keywords" content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co" />
    <meta name="generator" content="mRaovat" />
    <meta property="og:site_name" content="mraovat.vn" />

    <meta property="og:image" content="{{$categories[1]->links->image}}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="{{$categories[1]->forum_title}}" />
    <meta property="og:description" content="{{$categories[1]->forum_title}},{{$categories[0]->forum_title}},{{$categories[3]->forum_title}},{{$categories[4]->forum_title}},
    {{$categories[5]->forum_title}},{{$categories[6]->forum_title}},{{$categories[7]->forum_title}},{{$categories[8]->forum_title}},{{$categories[9]->forum_title}}" />
    <meta property="fb:app_id" content="295948790555485" />

@stop
@section('content')
    @include('includes.search')<!--include search form-->
    <div class="container cate">
        <div class="row" id="grid">
            <div class="col-sm-12">
                <div id="banner0" class="banner row">
                    @foreach ($categories as $cate)
                        <div class="col-sm-4">
                            <div class="product-item-cate">
                                <a href="{{URL::to('rao-vat')}}/{{AppHelper::RemoveUtf($cate->forum_title).'-'.$cate->forum_id}}">
                                    <img id="cate{{$cate->forum_id}}" src="{{$cate->links->image}}" alt="..." class="img-responsive">
                                </a>
                                <div class="category-title carousel-caption">
                                    <a href="{{URL::to('rao-vat')}}/{{AppHelper::RemoveUtf($cate->forum_title).'-'.$cate->forum_id}}"
                                       class="text-center" style="text-decoration: none;">
                                        <h4 class="h4-item">{{$cate->forum_title}}</h4>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('includes.social')
@stop