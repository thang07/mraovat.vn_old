@extends('layouts.main')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
    <meta property="og:site_name" content="mraovat.vn"/>

    <meta property="og:image" content="{{$categories[1]->links->image}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:title" content="{{$categories[1]->forum_title}}"/>
    <meta property="og:description"
          content="{{$categories[1]->forum_title}},{{$categories[0]->forum_title}},{{$categories[3]->forum_title}},{{$categories[4]->forum_title}},
    {{$categories[5]->forum_title}},{{$categories[6]->forum_title}},{{$categories[7]->forum_title}},{{$categories[8]->forum_title}},{{$categories[9]->forum_title}}"/>
    <meta property="fb:app_id" content="295948790555485"/>

@stop
@section('content')
    <div class="container cate">
        <div class="row cate2" id="grid">
            <div class="col-md-12">
                <div class="col-sm-3 col-md-3 ">

                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class=" navbar-brand">DANH MỤC</span>
                            </div>
                            <div class="navbar-collapse collapse sidebar-navbar-collapse">

                                <ul class="cate-left nav navbar-nav">
                                    @foreach ($categories as $cate)
                                        <li>

                                            <a href="{{URL::to('rao-vat')}}/{{AppHelper::RemoveUtf($cate->forum_title).'-'.$cate->forum_id}}">
                                                <i class="glyphicon glyphicon-chevron-right"></i> <h6
                                                        class="h6-item">{{$cate->forum_title}}</h6>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </div>
                </div>

                <div class="col-sm-9 col-md-9">
                    <div class="cate-contents">
                        <h4 class="h4-title"><i class="glyphicon glyphicon-home space"></i> » <a
                                    href="{{URL::to('/rao-vat')}}" class="space">Rao vặt</a> » <a
                                    href="{{URL::to('rao-vat')}}/{{$url}}"
                                    class="space">{{AppHelper::GetCategoryNameUtf($categories,$id)}}</a>
                        </h4>

                        <div class="search">
                            @if($isMobile)
                                <form method="get" action="{{URL::to('tim-kiem')}}" accept-charset="UTF-8" role="form">
                                    <input class="form-control search-control" type="text" name="q"
                                           value="{{Session::get('key')}}"
                                           placeholder="Tìm Kiếm"/>
                                    <input type="submit" class="btn btn-danger mobile" value="Tìm">
                                    <select name="category" class="form-control">
                                        <option value="all">Tất cả</option>
                                        @foreach ($categories as $cate)
                                            @if($cate->forum_id==$id)
                                                <option value="{{$cate->forum_id}}"
                                                        selected>{{$cate->forum_title}}</option>
                                            @else
                                                <option value="{{$cate->forum_id}}">{{$cate->forum_title}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <select name="location" class="form-control location">
                                        <option value="all">Tất cả</option>
                                        @foreach ($locations as $loc)
                                            <option value="{{$loc->location_id}}">{{$loc->location_name}}</option>
                                        @endforeach
                                    </select>

                                </form>
                            @else
                                <form method="get" action="{{URL::to('tim-kiem')}}" accept-charset="UTF-8" role="form">
                                    <input class="form-control" type="text" name="q" value="{{Session::get('key')}}"
                                           placeholder="Tìm Kiếm"/>
                                    <select name="category" class="form-control">
                                        <option value="all">Tất cả danh mục</option>
                                        @foreach ($categories as $cate)
                                            @if($cate->forum_id==$id)
                                                <option value="{{$cate->forum_id}}"
                                                        selected>{{$cate->forum_title}}</option>
                                            @else
                                                <option value="{{$cate->forum_id}}">{{$cate->forum_title}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <select name="location" class="form-control">
                                        <option value="all">Tất cả địa điểm</option>
                                        @foreach ($locations as $loc)
                                            <option value="{{$loc->location_id}}">{{$loc->location_name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="submit" class="btn btn-danger deskop" value="Tìm">
                                </form>
                            @endif
                        </div>

                        <div role="tabpanel" id="tabbar">
                            <ul role="tablist" class="nav nav-tabs">
                                <li class="total-item"><strong>Tất cả </strong>: {{number_format(Session::get('total'))}} tin</li>
                                <li class="tab" id="liList" role="presentation">
                                    <a href="" id="viewList">
                                        <span aria-hidden="true" class="glyphicon glyphicon-align-justify order"></span>
                                    </a>
                                </li>
                                <li class="tab" id="liGrid" role="presentation">
                                    <a href="" id="viewGrid">
                                        <span aria-hidden="true" class="glyphicon glyphicon-list order"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                @if(Session::has('view')&&Session::get('view')=='list')
                                    <div id="home" class="tab-pane active" role="tabpanel">
                                        <table class="table lists table-hover" id="tblList">
                                            <tr>
                                                <td class="td-timer">1 phút trước</td>
                                                <td class="product-name"><a href="#"><h4 class="media-heading vip">Tin
                                                            dành cho Vip</h4></a></td>
                                                <td><span class="price">Liên hệ</span></td>
                                            </tr>
                                            <tr>
                                                <td class="td-timer">1 phút trước</td>
                                                <td class="product-name"><a href="#"><h4 class="media-heading vip">Tin
                                                            dành cho Vip</h4></a></td>
                                                <td><span class="price">Liên hệ</span></td>
                                            </tr>

                                            @include('includes.itemList')

                                        </table>
                                    </div>
                                @else
                                    <div id="profile" class="tab-pane active" role="tabpanel">
                                        <div class="table grids">
                                            <div class="row" id="tblGrid">
                                                <div class="col-md-12 item">
                                                    <div class="col-md-10 product">
                                                        <div class="list">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                        <img class="media-object img-responsive"
                                                                             height="70px"
                                                                             width="90px"
                                                                             src="http://img.mraovat.vn/attachments/1431513624956-jpg.2235323"
                                                                             alt="img">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h4 class="media-heading">
                                                                        Tin dành cho Vip
                                                                    </h4>

                                                                    <div class="content">
                                                                        <div class="price">Liên hệ</div>
                                                                        <div class="address">mraovat.vn</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <span class="timer">1 phút trước</span>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 item">
                                                    <div class="col-md-10 product">
                                                        <div class="list">
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                        <img class="media-object img-responsive"
                                                                             height="70px"
                                                                             width="90px"
                                                                             src="http://img.mraovat.vn/attachments/1431513624956-jpg.2235323"
                                                                             alt="img">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h4 class="media-heading">
                                                                        Tin dành cho Vip
                                                                    </h4>

                                                                    <div class="content">
                                                                        <div class="price">Liên hệ</div>
                                                                        <div class="address">mraovat.vn</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <span class="timer">1 phút trước</span>
                                                    </div>
                                                </div>
                                                @include('includes.itemListImg')
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="animation_image" style="display:none" align="center"><img
                                        src="{{ URL::to('images/ajax-loader.gif') }}"
                                        alt="ajax-loader"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('includes.login')

    </div>
    @include('includes.social')
@stop
@section('footer')
    <script>
        $(document).ready(function () {
            var location = window.location + "";
            var view = "{{Session::get('view')}}";
            var isSearch = '{{$isSearch}}';
            var id = '{{$id}}';
            var loading = false;
            var page = 2;
            if (view == "") {
                $('#liGrid').addClass('disabled');
                $('#liList').removeClass('disabled');
                if (isSearch == 'true') {
                    $('#viewList').attr("href", location + "&view=list");
                    $('#viewGrid').attr("href", location + "&view=grid");
                } else {
                    $('#viewList').attr("href", location + "?view=list");
                    $('#viewGrid').attr("href", location + "?view=grid");
                }
            } else {
                if (view == 'grid') {
                    $('#liList').removeClass('disabled');
                    $('#liGrid').addClass('disabled');
                } else {
                    $('#liGrid').removeClass('disabled');
                    $('#liList').addClass('disabled');
                }
                var index = location.indexOf("view");
                if (index < 0) {
                    if (isSearch == 'true') {
                        $('#viewList').attr("href", location + "&view=list");
                        $('#viewGrid').attr("href", location + "&view=grid");
                    } else {
                        $('#viewList').attr("href", location + "?view=list");
                        $('#viewGrid').attr("href", location + "?view=grid");
                    }
                } else {
                    var url = location.substring(0, index);
                    $('#viewList').attr("href", url + "view=list");
                    $('#viewGrid').attr("href", url + "view=grid");
                }
            }

            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 800) {
                    if (loading == false) {
                        loading = true;
                        $('.animation_image').show();
                        if (isSearch == 'true') {
                            $.ajax({
                                type: "GET",
                                url: "{{URL::to('/load-more-search')}}",
                                data: {
                                    'page': page,
                                    'view': view
                                },
                                cache: false,
                                success: function (data) {
//                                    alert("Load more search: "+page + "-view:" + view + "-id:" + id + "-isSearch:" + isSearch);
                                    if (view == "grid") {
                                        $("#tblGrid").append(data);
                                    } else {
                                        $("#tblList").append(data);
                                    }
                                    $('.animation_image').hide();
                                    page++;
                                    loading = false;
                                },
                                error: function (xhr, status, error) {
                                    //alert(xhr.responseText);
                                    loading = true;
                                    $('.animation_image').hide();
                                }
                            });
                        } else {
                            $.ajax({
                                type: "GET",
                                url: "{{URL::to('/load-more')}}",
                                data: {
                                    'id': id,
                                    'page': page,
                                    'view': view
                                },
                                cache: false,
                                success: function (data) {
//                                    alert(page + "-view:" + view + "-id:" + id + "-isSearch:" + isSearch);
                                    if (view == "grid") {
                                        $("#tblGrid").append(data);
                                    } else {
                                        $("#tblList").append(data);
                                    }
                                    $('.animation_image').hide();
                                    page++;
                                    loading = false;
                                },
                                error: function (xhr, status, error) {
                                    //alert(xhr.responseText);
                                    loading = true;
                                    $('.animation_image').hide();
                                }
                            });
                        }
                    }
                }
            });
        });
        {{--end document ready--}}
    </script>
@stop