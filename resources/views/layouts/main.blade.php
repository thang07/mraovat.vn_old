<!DOCTYPE html>
<html lang="en">
<head>

    @include('includes.menu')
</head>
<body class="bg main-page">
<div id="content" class="wrapper-content">
    @yield('content')
</div>
<div id="footer">
    @include('includes.footer')
</div>
</body>
</html>
