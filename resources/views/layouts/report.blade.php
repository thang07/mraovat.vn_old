<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.headerReport')
</head>
<body class="bg">
<div id="content" class="wrapper-content">
    @yield('content')
</div>
<div id="footer">
    @include('includes.footer')
</div>
</body>
</html>
