@extends('layouts.home')
@section('title')
    mRaovat - mạng rao vặt tốt nhất Việt Nam
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn" />
    <meta name="keywords" content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co" />
    <meta name="generator" content="mRaovat" />
    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="http://mraovat.vn/images/img-mobile.png" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="BÁN LIỀN TAY MUA TRONG NGÀY" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />
@stop
@section('content')
    <div class="welcome-content">
        <div class="welcome-slide">
            <div class="container ads">
                <strong class="h1">BÁN LIỀN TAY<br>
                    MUA TRONG NGÀY</strong>

                <p class="message">mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh
                    mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like
                    ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng
                </p>
            </div>
        </div>
        <div class="video-header-bg"></div>
        <div class="welcome-video">
            <div class="container">
                <object width="80%" height="440px"
                        data="//www.youtube.com/embed/mOsceXOxE_Q?autoplay=0&showinfo=0&controls=0&rel=0">
                </object>
            </div>
        </div>
        <div class="welcome-video-ft"></div>
        <div class="welcome-about-us">
            <div class="container">
                <h1 class="h1">GIỚI THIỆU</h1>

                <div class="row">
                    <div class="col-md-6">
                        <ul class="menu-about-us">
                            <li><img alt="icon" src="/images/ico-1.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h2 class="h2">SẢN PHẨM ĐA DẠNG</h2>

                                    <p>- Đầy đủ tất cả các loại hàng hóa: đồ gia dụng, điện tử,
                                        thời trang, mẹ và bé...</p>

                                    <p>- Hàng hóa được sắp sếp theo từng gian hàng tiện dụng
                                        cho người bán và người mua.</p>
                                </div>
                            </li>
                            <li><img alt="icon" src="/images/ico-2.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h4 class="h2">TÌM KIẾM DỄ DÀNG</h4>

                                    <p>- Tìm sản phẩm theo từ khóa</p>

                                    <p>- Tìm những sản phẩm đang được rao bán xung quanh mình, chức năng "Gầntôi"</p>
                                </div>
                            </li>
                            <li><img alt="icon" src="/images/ico-3.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h4 class="h2">MUA BÁN THUÂN TIỆN</h4>

                                    <p>- Mua bán mọi lúc mọi nơi</p>

                                    <p>- Rao bán trong 30s, chụp hình, mô tả sản phẩm, đăng tin</p>

                                    <p>- Cập nhật thông tin liên tục</p>
                                </div>
                            </li>
                            <li><img alt="icon" src="/images/ico-4.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h4 class="h2">TƯƠNG TÁC NHANH CHÓNG</h4>

                                    <p>- tích hợp công cụ tương tác like, share, comment, gọi điện, nhắn tin, email...
                                        ngay
                                        bên
                                        dưới sản phẩm</p>

                                    <p>- Xuất hiện thông báo mỗi khi có tương tác, chức năng "Thông báo"</p>
                                </div>
                            </li>
                            <li><img alt="icon" src="/images/ico-5.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h4 class="h2">SỬ DỤNG MIỄN PHÍ</h4>

                                    <p>- Ứng dụng có mặt trên 3 hệ điều hành: iOS, Android, Windows Phone</p>

                                    <p>- Cài đặt và sử dụng miễn phí</p>
                                </div>
                            </li>
                            <li><img alt="icon" src="/images/ico-6.png" class="img-responsive icon"/>

                                <div class="list-group">
                                    <h4 class="h2">HỖ TRỢ TỐI ĐA</h4>

                                    <p>- Chỉnh sửa thông tin cho hợp lý</p>

                                    <p>- Hỗ trợ kết nối miễn phí giữa người mua- người bán</p>

                                    <p>- Giúp quảng bá sản phẩm của bạn đến mọi nới</p>
                                </div>

                            </li>
                        </ul>

                    </div>
                    <div class="col-md-6 img-phone">
                        <img src="/images/img-mobile.png" class="img-responsive" alt="Phone image"/>
                    </div>
                </div>
            </div>
        </div>
        @include('includes.social')
    </div>
@stop
