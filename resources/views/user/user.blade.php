@extends('layouts.home')
@section('title')
    Sản phẩm của {{$name}} trên mRaovat
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="{{$user->links->avatar}}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="BÁN LIỀN TAY MUA TRONG NGÀY" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="user">
                    <p>
                        <img src="{{$user->links->avatar}}" alt="user" class="img-circle img-user" width="150px"
                             height="150px">
                    </p>

                    <p>

                    <h1><strong>{{$user->user_title}}</strong></h1>
                    </p>
                    @if(strlen($user->user_location)>0)
                        <p>

                            <span class="glyphicon glyphicon-map-marker"></span><a class="btn btn-link" target="_blank"
                                                                                   href="http://maps.google.com/?q={{$user->user_location}}">{{$user->user_location}}
                            </a>
                        </p>
                    @endif
                    <p>
                        <span class="glyphicon glyphicon-link"></span><a class="btn btn-link" target="_blank"
                                                                         href="{{$user->links->permalink}}">
                            {{$user->links->permalink}}</a>
                    </p>

                    <p>

                    <div class="fb-like" data-href="{{URL::current()}}" data-width="100px" data-layout="button_count"
                         data-action="like" data-show-faces="true" data-share="true"></div>
                    </p>
                    <br>

                    <div>
                        <div class="well well-sm col-md-3">{{$total}} Post</div>
                        <div class="well well-sm col-md-3">{{$user->user_like_count}} Likes</div>
                        <div class="well well-sm col-md-3">{{$user->followers_count}} Follower</div>
                        <div class="well well-sm col-md-3">{{$user->followings_count}} Following</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="banner0" class="banner row">
                    @include('includes.userItem')
                </div>
            </div>
        </div>
        <div class="animation_image" style="display:none" align="center"><img src="{{ URL::to('images/ajax-loader.gif') }}"
                                                                              alt="ajax-loader"/>
        </div>
    </div>
    @include('includes.social')
@stop
@section('footer')
    <script>
        var loading = false;
        var page = 2;
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 800) {
                if (loading == false) {
                    loading = true;
                    $('.animation_image').show();
                    $.ajax({
                        type: "GET",
                        url: "{{URL::to('/load-more-thread-user')}}",
                        data: {'id':'{{$user->user_id}}', 'page': page},
                        cache: false,
                        success: function (data) {
                            $("#banner0").append(data);
                            $('.animation_image').hide();
                            page++;
                            loading = false;
                        },
                        error: function (xhr, status, error) {
                            //alert(xhr.responseText);
                            loading = true;
                            $('.animation_image').hide();
                        }
                    });
                }
            }
        });
    </script>
@stop
