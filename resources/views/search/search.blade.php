@extends('layouts.home')
@section('title')
    Kết quả tìm kiếm cho {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>

@stop
@section('content')
    @include('includes.search')
    @include('includes.grid')
    @include('includes.social')
@stop
@section('footer')
    <script>
        var loading = false;
        var page = 2;
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 800) {
                if (loading == false) {
                    loading = true;
                    $('.animation_image').show();
                    $.ajax({
                        type: "GET",
                        url: "{{URL::to('/load-more-search')}}",
                        data: {'page': page},
                        cache: false,
                        success: function (data) {
                            $("#banner0").append(data);
                            $('.animation_image').hide();
                            page++;
                            loading = false;
                        },
                        error: function (xhr, status, error) {
                            //alert(xhr.responseText);
                            loading = true;
                            $('.animation_image').hide();
                        }
                    });
                }
            }
        });
    </script>
@stop
