<title>@yield('title')</title>
<link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('/css/normalize.css')}}" rel="stylesheet">
<link href="{{asset('/css/style.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="{{URL::to('favicon.png')}}">
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-27518912-8', 'auto');
    ga('send', 'pageview');

    ///begin Like-facebook
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    //end like box facebook
</script>

@yield('head')
<nav class="navbar navbar-default menu-top" role="navigation" id="header-site">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#mraovat">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}" class="logo-site"><img src="/images/logo.png"
                                                                                   class="img-responsive"
                                                                                   alt="logo"></a>
        </div>
        <div class="collapse navbar-collapse mraomenu" id="mraovat">
            <ul id="subnav" class="nav navbar-nav navbar-right">
                <li><a id="trang-chu" href="{{URL::to('/trang-chu')}}">Trang chủ</a></li>
                <li><a id="rao-vat" href="{{URL::to('/rao-vat')}}">Rao vặt</a></li>
                <li><a id="blog" href="{{URL::to('/blog')}}">Blog</a></li>
                <li><a id="huong-dan" href="{{URL::to('/huong-dan')}}">Hướng dẫn</a></li>
                <li><a id="lien-he" href="{{URL::to('/lien-he')}}">Liên hệ</a></li>
            </ul>
        </div>
    </div>
</nav>