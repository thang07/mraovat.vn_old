@foreach ($threads as $item)
    <tr>
        <?php \Jenssegers\Date\Date::setLocale('vi'); $date = new \Jenssegers\Date\Date($item->thread_create_date, new DateTimeZone('Asia/Ho_Chi_Minh'));?>
        <td class="td-timer">{{$date->ago()}}</td>
        <td class="product-name">
           <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
               <h4 class="media-heading"> {{$item->thread_title}}</h4>
           </a>
        </td>
        <td class="td-price"><span class="price">{{number_format($item->first_post->post_price)}} vnd</span></td>
    </tr>
@endforeach