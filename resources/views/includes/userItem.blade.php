@foreach ($threads as $item)
    <div class="col-sm-4">
        <div class="product-item">
            @if($item->first_post->post_attachment_count >0 )
                @if($isMobile)
                    <div class="item-container">
                        <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                            <img src="{{$item->first_post->attachments[0]->links->thumbnail}}" alt="..."
                                 class="img-responsive"/></a>
                    </div>
                @else
                    <div class="item-container">
                        <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                            <img src="{{$item->first_post->attachments[0]->links->permalink}}" alt="..."
                                 class="img-responsive"/></a>
                    </div>
                @endif
            @else
                <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                    <img src="{{URL::to('images/placeholder.png')}}" alt="..." class="img-responsive">
                </a>
            @endif
            <div class="caption details-item">
                <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}"
                   class="item-title ">
                    <h4 class="product-item-crop-title item"> {{$item->thread_title}}</h4>
                </a>

                <p class="price item">{{number_format($item->first_post->post_price)}} vnd</p>
                <?php \Jenssegers\Date\Date::setLocale('vi'); $date = new \Jenssegers\Date\Date($item->thread_create_date, new DateTimeZone('Asia/Ho_Chi_Minh'));?>
                <p class="price item ago">{{$date->ago()}}</p>

                <div class="icon-group text-center">
                    <label class="col-xs-4  first">
                        <i class="glyphicon glyphicon-heart"></i><span
                                class="number"> {{$item->first_post->post_like_count}}</span>
                    </label>
                    <label href="#" class="col-xs-4 ">
                        <i class="glyphicon glyphicon-comment"></i><span
                                class="number"> {{$item->thread_post_count-1}}</span></label>
                    <label href="#" class="col-xs-4">
                        <i class="glyphicon glyphicon-eye-open"></i><span
                                class="number"> {{$item->thread_view_count}}</span></label>
                </div>
            </div>
        </div>
    </div>
@endforeach