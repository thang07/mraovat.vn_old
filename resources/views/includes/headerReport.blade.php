<title>@yield('title')</title>
<link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('/css/normalize.css')}}" rel="stylesheet">
<link href="{{asset('/css/style.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="{{URL::to('favicon.png')}}">


@yield('head')
<nav class="navbar navbar-default menu-top" role="navigation" id="header-site">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#mraovat">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}" class="logo-site"><img src="/images/logo.png"
                                                                                   class="img-responsive"
                                                                                   alt="logo"></a>
        </div>
        <div class="collapse navbar-collapse mraomenu" id="mraovat">
            <ul id="subnav" class="nav navbar-nav navbar-right">
                <li><a id="day" href="{{URL::to('/report/day')}}">Ngày</a></li>
                <li><a id="week" href="{{URL::to('/report/week')}}">Tuần</a></li>
                <li><a id="month" href="{{URL::to('/report/month')}}">Tháng</a></li>
            </ul>
        </div>
    </div>
</nav>