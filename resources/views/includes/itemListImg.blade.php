@foreach ($threads as $item)
    <div class="col-md-12 item">

        <div class="col-md-10 product">
            <div class="list">
                <div class="media">
                    <div class="media-left">

                            @if($item->first_post->post_attachment_count >0&&isset($item->first_post->attachments))
                                @if($isMobile)
                                    <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                                        <img src="{{$item->first_post->attachments[0]->links->thumbnail}}" alt="..."
                                             class="media-object img-responsive" height="70px"
                                             width="90px"/></a>
                                @else
                                    <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">

                                        <img src="{{$item->first_post->attachments[0]->links->permalink}}" alt="..."
                                             class="media-object img-responsive" height="70px"
                                             width="90px"/>

                                    </a>
                                @endif
                            @else
                                <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                                    <img src="{{URL::to('images/placeholder.png')}}" alt="..." class="media-object img-responsive" height="70px"
                                         width="90px">
                                </a>
                            @endif

                    </div>
                    <div class="media-body">
                        <a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">
                            <h4 class="media-heading">
                                {{$item->thread_title}}
                            </h4>
                        </a>
                        <div class="content">
                            <div class="price">{{number_format($item->first_post->post_price)}} vnd</div>
                            <div class="address"><a href="http://maps.google.com/?q={{$item->first_post->post_address}}" target="_blank">{{$item->first_post->post_address}}</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <?php \Jenssegers\Date\Date::setLocale('vi'); $date = new \Jenssegers\Date\Date($item->thread_create_date, new DateTimeZone('Asia/Ho_Chi_Minh'));?>
            <span class="timer">{{$date->ago()}}</span>
        </div>
    </div>

    {{--<tr>--}}
        {{--<td>--}}
            {{--<div class="list">--}}
                {{--<div class="media">--}}
                    {{--<div class="media-left">--}}
                        {{--<a href="#">--}}
                            {{--@if($item->first_post->post_attachment_count >0&&isset($item->first_post->attachments))--}}
                                {{--@if($isMobile)--}}
                                        {{--<a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">--}}
                                            {{--<img src="{{$item->first_post->attachments[0]->links->thumbnail}}" alt="..."--}}
                                                 {{--class="media-object img-responsive" height="70px"--}}
                                                 {{--width="90px"/></a>--}}
                                {{--@else--}}
                                        {{--<a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">--}}

                                            {{--<img src="{{$item->first_post->attachments[0]->links->permalink}}" alt="..."--}}
                                                 {{--class="media-object img-responsive" height="70px"--}}
                                                 {{--width="90px"/>--}}

                                        {{--</a>--}}
                                {{--@endif--}}
                            {{--@else--}}
                                {{--<a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">--}}
                                    {{--<img src="{{URL::to('images/placeholder.png')}}" alt="..." class="media-object img-responsive" height="70px"--}}
                                         {{--width="90px">--}}
                                {{--</a>--}}
                            {{--@endif--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="media-body">--}}
                        {{--<a href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories,$item->forum_id))}}/{{AppHelper::RemoveUtf($item->thread_title)}}-{{$item->thread_id}}">--}}
                            {{--<h4 class="media-heading">--}}
                                {{--{{$item->thread_title}}--}}
                            {{--</h4>--}}
                        {{--</a>--}}
                        {{--<div class="content">--}}
                            {{--<div class="price">{{number_format($item->first_post->post_price)}} vnd</div>--}}
                            {{--<div class="address"><a href="http://maps.google.com/?q={{$item->first_post->post_address}}" target="_blank">{{$item->first_post->post_address}}</a></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--<td class="td-gird-timer"><span class="timer">{{$date->ago()}}</span></td>--}}
    {{--</tr>--}}
@endforeach
