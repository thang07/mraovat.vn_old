<div class="search-login-register-form">
    <div class="container container-search">
        <nav class="navbar-default" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#login-mraovat">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-form navbar-left" role="search">
                    {{--<form method="GET" action="{{URL::to('tim-kiem')}}" accept-charset="UTF-8">--}}
                        {{--<input placeholder="Tìm kiếm" class="form-control" name="q" type="text" value="">--}}
                        {{--@if(isset($id))--}}
                        {{--<input name="id" type="hidden" value="{{$id}}">--}}
                        {{--@endif--}}
                    {{--</form>--}}
                    {{--@if(@Session::has('result'))--}}
                        {{--<p>{!!Session::get('result')!!}</p>--}}
                    {{--@endif--}}
                </div>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse login-mraovat" id="login-mraovat">
                <ul class="nav navbar-nav navbar-right action-box">
                    @if($isMobile)
                        <li><a id="mobileNews" href="#" data-toggle="modal" data-target="#downloadBox"><i
                                        class="glyphicon-class glyphicon glyphicon-paperclip "></i> Đăng tin miễn
                                phí</a></li>
                    @else
                        <li><a href="#" id="postNews"><i class="glyphicon-class glyphicon glyphicon-paperclip "></i> Đăng
                                tin miễn phí</a></li>
                    @endif

                    @if(@Session::has('userInfo'))
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown"><img width="30px" height="30px" class="img-circle"
                                                                    src="{{@Session::get('userInfo')->links->avatar}}"/>
                                {{@Session::get('userInfo')->user_title}} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a style="color: #000000!important;"
                                       href="{{URL::to(@Session::get('userInfo')->links->permalink)}}">Gian hàng của
                                        tôi</a>
                                </li>
                                <li><a style="color: #000000!important;" href="#">Đổi mật khẩu</a></li>
                                <li><a style="color: #000000!important;"
                                       href="{{URL::to('logout')}}">Đăng xuất</a></li>
                            </ul>
                        </li>
                    @else
                        <li><a href="#" data-toggle="modal" data-target="#loginBox"><i
                                        class="glyphicon glyphicon-user"></i> Đăng nhập</a></li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->

        </nav>
    </div>
</div>

<div class="modal fade" id="downloadBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center">
                <h3>Ứng dụng mRaovat được tải miễn phí trên 3 kho ứng dụng,để đăng tin miễn phí trên mRaovat bạn cần tải
                    ứng dụng này</h3>
                <a class="btn btn-lg btn-primary" href="{{URL::to('download')}}">Tải ngay</a>
                <a class="btn btn-lg btn-danger" data-dismiss="modal">Không</a>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- Login Modal -->
<div class="modal fade" id="loginBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title text-center" id="myModalLabel">Đăng nhập</h1>
            </div>
            <div class="modal-body text-center">
                <!--                <div class="fb-login-button" scope="public_profile,email" data-max-rows="1" data-size="xlarge" data-show-faces="false"-->
                <!--                     data-auto-logout-link="false"></div>-->
                <!--                <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">-->
                <!--                </fb:login-button>-->
                <a class="btn btn-lg btn-primary"
                   href="https://www.facebook.com/dialog/oauth?client_id=295948790555485&redirect_uri={{URL::to('login/fb')}}">Login
                    with Facebook</a>
                <a class="btn btn-lg btn-danger" title="Login with account google"
                   href="https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri={{URL::to('login/gg')}}&client_id=292808066700-2t5eebotf7b0pkllkdmhkd2u11p7tp76.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email">
                    Login with Google</a>
                </span>
                </span>
                <div id="status"></div>
                <p>Hoặc</p>
                @if(Session::get('err')=='login')
                    @if (count($errors) > 0)
                @section('footer')
                    <script type="text/javascript">
                        $(function () {
                            $('#loginBox').modal('show');
                        });
                    </script>
                @stop
                <div class="alert alert-danger">
                    {{--<strong>Ô!</strong> đã có vấn đề với giá trị nhập của bạn.<br><br>--}}
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @endif
                <form method="POST" action="{{URL::to('login')}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="email@gmail.com" class="form-control" name="email" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Mật khẩu" class="form-control" name="password" type="password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="btn btn-default" type="submit" value="Đăng nhập">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" data-toggle="modal" data-dismiss="modal" data-target="#registerBox">Đăng ký</a>
            </div>
        </div>
    </div>
</div>

<!-- Register Modal -->
<div class="modal fade" id="registerBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="myModalLabel">Đăng ký</h4>
            </div>
            <div class="modal-body text-center">
                @if(Session::get('err')=='register')
                    @if (count($errors) > 0)
                @section('footer')
                    <script type="text/javascript">
                        $(function () {
                            $('#registerBox').modal('show');
                        });
                    </script>
                @stop
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @endif
                <form method="POST" action="{{URL::to('register')}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="email@gmail.com" class="form-control" name="email" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Mật khẩu" class="form-control" name="password" type="password" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Nhập lại mật khẩu" class="form-control" name="repassword"
                                   type="password" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Tên hiển thị" class="form-control" name="dispname" type="text" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Thành phố" class="form-control" name="city" type="text" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input placeholder="Điện thoại" class="form-control" name="phone" type="text" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="btn btn-default" type="submit" value="Xong">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>