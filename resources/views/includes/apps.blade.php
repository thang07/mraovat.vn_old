<div class="apps-mraovat">
    <div class="header-bg"></div>
    <div class="container text-center">
        <div class="col-sm-4">
            <a href="https://play.google.com/store/apps/details?id=com.lms.mraovat" target="_blank"><img
                    src="/images/ico-android.png" alt="android"></a>
            <a href="https://play.google.com/store/apps/details?id=com.lms.mraovat" target="_blank" class="btn" type="button">
               TẢI NGAY
            </a>
        </div>
        <div class="col-sm-4">
            <a href="https://itunes.apple.com/vi/app/mraovat/id861916721?mt=8" target="_blank"><img
                    src="/images/ico-ios.png" alt="ios"></a>
            <a href="https://itunes.apple.com/vi/app/mraovat/id861916721?mt=8" target="_blank" class="btn" type="button">TẢI NGAY</a>
        </div>
        <div class="col-sm-4">
            <a href="http://www.windowsphone.com/en-us/store/app/mraovat/22cef701-cf53-4b46-8186-6f416368e15b"
               target="_blank"><img src="/images/ico-wp.png" alt="window"></a>
            <a href="http://www.windowsphone.com/en-us/store/app/mraovat/22cef701-cf53-4b46-8186-6f416368e15b" target="_blank" class="btn" type="button">TẢI NGAY</a>
        </div>
    </div>
</div>
