@include('includes.apps')
<div class="copyright-footer">
    <div id="copyright" class="text-center"><p>© 2014 mRaovat – Bản quyền của
            CÔNG TY CỔ PHẦN CÔNG NGHỆ THANH VÂN<br>114 đường số 2, cư xã Đô Thành, Phường 04, Quận 03, thành phố Hồ Chí
            Minh<br>Giấy chứng nhận đăng ký kinh doanh số 0313 011874<br/> do Sở Kế hoạch và Đầu tư Tp HCM
            cấp.</p>
        <ul class="list-inline">
            <li class="first"><a href="{{URL::to('about-us')}}">Về chúng tôi</a></li>
            <li><a href="{{URL::to('policy')}}" target="_blank">Điều khoản</a></li>
        </ul>
        <ul class="list-inline">
            <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=10918" target="_blank">
                <img title=""  src="http://online.gov.vn/seals/i36BzNWWzqJfIqV0lDMUTA==.jpgx" alt="gov"/></a>
        </ul>
    </div>
</div>


<script src="{{asset('js/jquery-1.9.0.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<div id="top-link-block" class="hidden">
    <a href="#top" class="well well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </a>
</div>
<script>
    $("#postNews").click(function () {
        alert('Bạn cần cài đặt mRaovat trên mobile để sử dụng chức năng này');
    });

    $("#from").keyup(function (e) {
        var to=$("#from").val().toString().replace(/,/g, "");
        $("#from").val(numberWithCommas(to));
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    $("#to").keyup(function (e) {
        var to=$("#to").val().toString().replace(/,/g, "");
        $("#to").val(numberWithCommas(to));
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    if (($(window).height() + 100) < $(document).height()) {
        $('#top-link-block').removeClass('hidden').affix({
            offset: {top: 100}
        });
    }

    var str = window.location + '';
    $('#subnav a').each(function (index) {
        var n=str.search(this.id);
        if(n!=-1)
        {
            //$(this).parent().addClass("active");
        }
    });

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-27518912-8', 'auto');
    ga('send', 'pageview');

    ///begin Like-facebook
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    //end like box facebook

</script>
@yield('footer')

