<title>@yield('title')</title>
<link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('/css/normalize.css')}}" rel="stylesheet">
<link href="{{asset('/css/style.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="{{URL::to('favicon.png')}}">


@yield('head')
<nav class="navbar navbar-default menu-top navbar-fixed-top" role="navigation" id="header-site">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#mraovat">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}" class="logo-site"><img src="/images/logo.png"
                                                                                   class="img-responsive"
                                                                                   alt="logo"></a>
        </div>
        <div class="collapse navbar-collapse mraomenu" id="mraovat">
            <ul id="subnav" class="nav navbar-nav navbar-right">
                @if($isMobile)
                    <li><a id="mobileNews" href="#" data-toggle="modal" data-target="#downloadBox"><i
                                    class="glyphicon-class glyphicon glyphicon-paperclip "></i> Đăng tin miễn
                            phí</a></li>
                @else
                    <li><a href="#" id="postNews"><i class="glyphicon-class glyphicon glyphicon-paperclip "></i> Đăng
                            tin miễn phí</a></li>
                @endif

                @if(@Session::has('userInfo'))
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown"><img width="30px" height="30px" class="img-circle"
                                                                src="{{@Session::get('userInfo')->links->avatar}}"/>
                            {{@Session::get('userInfo')->user_title}} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a style="color: #000000!important;"
                                   href="{{URL::to(@Session::get('userInfo')->links->permalink)}}">Gian hàng của
                                    tôi</a>
                            </li>
                            <li><a style="color: #000000!important;" href="#">Đổi mật khẩu</a></li>
                            <li><a style="color: #000000!important;"
                                   href="{{URL::to('logout')}}">Đăng xuất</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="#" data-toggle="modal" data-target="#loginBox"><i
                                    class="glyphicon glyphicon-user"></i> Đăng nhập</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>