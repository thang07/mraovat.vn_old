@extends('layouts.home')
@section('title')
    mRaovat - Chính sách sử dụng
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
@stop
@section('content')
    <div class="container policy">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center" class="font-Light" style="font-weight: bold;">QUY CHẾ HOẠT ĐỘNG CỦA SÀN GIAO DỊCH
                    THƯƠNG MẠI ĐIỆN TỬ WWW.MRAOVAT.VN</h2>

                <div class="font">
                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> là trung tâm thương mại điện tử
                        phục vụ nhu cầu giới thiệu, mua bán trực tuyến bao gồm: Đăng tin rao vặt sản phẩm, đặt banner
                        quảng cáo.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> hỗ trợ tối đa cho khách hàng muốn
                        tìm hiểu thông tin trực tuyến về nhiều loại sản phẩm, dịch vụ khác nhau hoặc có nhu cầu mua sắm
                        hàng hoá trực tuyến.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> được xây dựng với hai phiên bản:
                        website rao vặt truyền thống và ứng dụng mua bán trên Smartphone.
                    </p>

                    <p>
                        Sứ mệnh mà <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> hướng tới là sẽ trở
                        thành Sàn thương mại điện tử tin cậy trong thị trường Thương mại điện tử, là cầu nối thương mại
                        giữa người bán và người mua, giữa doanh nghiệp với doanh nghiệp.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">I. Nguyên tắc chung</p>

                    <p>
                        Sàn giao dịch điện tử <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> do Công ty
                        cổ phần công nghệ Thanh Vân (Công

                        ty) thực hiện hoạt động và vận hành. Thành viên trên sàn giao dịch điện tử là các thương

                        nhân, tổ chức, cá nhân có hoạt động thương mại hợp pháp được <a href="http://mraovat.vn"
                                                                                        target="_blank">www.mraovat.vn</a>
                        chính

                        thức công nhận và được phép sử dụng dịch vụ do Sàn giao dịch điện tử <a href="http://mraovat.vn"
                                                                                                target="_blank">www.mraovat.vn</a>

                        và các bên liên quan cung cấp.</p>

                    <p>
                        Nguyên tắc này áp dụng cho các thành viên đăng ký sử dụng, tham gia đăng tin sản phẩm

                        được thực hiện trên Sàn giao dịch Thương mại điện tử <a href="http://mraovat.vn"
                                                                                target="_blank">www.mraovat.vn.</a>
                    </p>

                    <p>
                        Thương nhân, tổ chức, cá nhân tham gia giao dịch tại Sàn Giao dịch thương mại điện tử

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> tự do thỏa thuận trên cơ sở tôn
                        trọng quyền và lợi ích hợp pháp của các

                        bên tham gia hoạt động mua bán sản phẩm, dịch vụ thông qua hợp đồng, không trái với

                        qui định của pháp luật.
                    </p>

                    <p>
                        Thông tin về thương nhân, tổ chức, cá nhân tham gia là thành viên trên <a
                                href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>

                        phải minh bạch và chính xác.
                    </p>

                    <p>
                        Sản phẩm, dịch vụ tham gia giao dịch trên Sàn giao dịch thương mại điện tử

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> phải đáp ứng đầy đủ các quy định
                        của pháp luật có liên quan, không

                        thuộc các trường hợp cấm kinh doanh, cấm quảng cáo theo quy định của pháp luật.
                    </p>

                    <p>
                        Hoạt động mua bán hàng hóa qua Sàn giao dịch thương mại điện tử <a href="http://mraovat.vn"
                                                                                           target="_blank">www.mraovat.vn</a>

                        phải được thực hiện công khai, minh bạch, đảm bảo quyền lợi của người tiêu dùng.
                    </p>

                    <p>
                        Tất cả các nội dung trong Quy chế này phải tuân thủ theo hệ thống pháp luật hiện hành

                        của Việt Nam. Thành viên khi tham gia vào Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                        target="_blank">www.mraovat.vn</a>
                        phải

                        tự tìm hiểu trách nhiệm pháp lý của mình đối với luật pháp hiện hành của Việt Nam

                        và cam kết thực hiện đúng những nội dung trong Quy chế của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn.</a>
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">II. Quy định chung</p>

                    <p>
                        - Tên Miền Sàn Giao dịch Thương mại Điện tử:
                    </p>

                    <p>
                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> do Công ty cổ
                        phần công nghệ Thanh Vân

                        phát triển với tên miền Sàn giao dịch là: <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        (sau đây gọi tắt là: “Sàn giao

                        dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>”).
                    </p>

                    <p>
                        Định nghĩa chung:
                    </p>

                    <p>
                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> : là website
                        cho phép các thương nhân, tổ

                        chức, cá nhân có nhu cầu đăng tin rao bán sản phẩm, dịch vụ do thương nhân, tổ chức, cá

                        nhân đó cũng cấp.
                    </p>

                    <p>
                        Người bán: là thương nhân, tổ chức, cá nhân có nhu cầu sử dụng dịch vụ của

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> bao gồm: đăng tin rao vặt trực
                        tuyến, đăng tin giới thiệu sản phẩm,

                        đăng tin khuyến mại sản phẩm/dịch vụ.
                    </p>

                    <p>
                        Người mua (khách hàng): là thương nhân, tổ chức, cá nhân có nhu cầu tìm hiểu

                        thông tin về sản phẩm, dịch vụ được đăng tải trên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        của người bán.

                        Người mua có quyền đăng ký tài khoản hoặc không cần đăng ký.
                    </p>

                    <p>Thành viên: là bao gồm cả người bán lẫn người mua.</p>

                    <p>- Thành viên tham gia giao dịch trên Sàn giao dịch <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        là thương

                        nhân, tổ chức, cá nhân có nhu cầu mua bán sản phẩm, dịch vụ trên website.</p>

                    <p>
                        - Thành viên phải đăng ký kê khai ban đầu các thông tin cá nhân có liên quan,

                        được Ban quản lý sàn TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> chính
                        thức công nhận và được phép sử

                        dụng dịch vụ do Sàn giao dịch TMĐT.
                    </p>

                    <p>- Khi bạn đăng ký là thành viên của <a href="http://mraovat.vn"
                                                              target="_blank">www.mraovat.vn</a>, thành viên hiểu rằng:
                    </p>

                    <p>
                        + Thành viên có thể tạo một tài khoản cá nhân của mình để sử dụng.</p>

                    <p>
                        + Thành viên có thể mua sản phẩm, dịch vụ theo đúng giá và quy chuẩn theo đúng

                        cam kết của thương nhân hợp pháp đã công bố trên sàn.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">III. Quy trình giao dịch</p>

                    <p style="font-weight: bold;font-size: 15px;">Quy trình dành cho người mua hàng</p>

                    <p>
                        Khi có nhu cầu mua hàng trên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        người mua nên thực hiện theo các

                        bước sau đây:
                    </p>

                    <p>
                        - Tìm kiếm, tham khảo thông tin sản phẩm, dịch vụ và các người bán mà người mua

                        đang quan tâm;
                    </p>

                    <p>
                        - Tham khảo thông tin giá và chính sách hỗ trợ của bên bán sản phẩm, dịch vụ mà

                        người mua đang có nhu cầu mua. (có thể tham khảo mặt hàng tương tự của những người

                        bán khác trên website <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> (để đưa ra
                        quyết định mua sản phẩm, dịch vụ đó);
                    </p>

                    <p>
                        - Dựa trên thông tin tham khảo được từ người bán sản phẩm, dịch vụ người mua liên

                        hệ với người bán qua thông tin liên hệ đăng tải trong nội dung tin bán hàng hóa để hỏi về

                        thêm thông tin sản phẩm, dịch vụ hoặc đến gặp trực tiếp người bán, tiếp xúc trực tiếp để

                        xem và mua sản phẩm, dịch vụ;
                    </p>

                    <p>
                        - Sau khi đã trao đổi được với người bán, người mua có thể quyết định đặt hàng;
                    </p>

                    <p>
                        - Người mua lựa chọn cách thức giao dịch (tuy vào thỏa thuận của người mua và

                        người bán)
                    </p>

                    <p>
                        - Người mua nhận sản phẩm, dịch vụ.
                    </p>

                    <p>
                        - Người mua thắc mắc, khiếu nại người bán (nếu có)
                    </p>

                    <p style="font-weight: bold;font-size: 15px;">Quy trình dành cho người bán hàng</p>

                    <p>
                        1. Chuẩn bị tin bài bằng chữ và hình ảnh:
                    </p>

                    <p>
                        Các tin bài cần đăng phải được chia thành 2 phần được phân theo định dạng bằng

                        chữ và hình ảnh.
                    </p>

                    <p>
                        Các nội dung bằng chữ nên được đánh máy sẵn trên một chương trình soạn thảo

                        văn bản ( MS Word, OpenOffice) theo định dạng font Arial, cỡ chữ 10.
                    </p>

                    <p>
                        Các nội dung bằng hình ảnh được định dạng theo dạng ảnh jpg,bmp, gif hoặc có

                        thể chụp trực tiếp sản phẩm khi đăng bán.
                    </p>

                    <p>
                        2. Đưa nội dung lên website:
                    </p>

                    <p>
                        - Để đưa Tin rao văt lên website, cần thực hiện tuần tự các bước sau:
                    </p>

                    <p style="font-style: italic;">
                        a, Đăng tin rao vặt qua website
                    </p>

                    <p>
                        Đăng nhập (hoặc không) với thông tin đã được đăng ký tại <a href="http://mraovat.vn"
                                                                                    target="_blank">www.mraovat.vn</a>

                        vào mục “<b>Đăng tin miễn phí</b>”, ở đây người bán sẽ viết bài liên quan đến sản phẩm (mô

                        tả chi tiết sản phẩm, hình ảnh, các chính sách liên quan như bảo hành, đổi trả, vận chuyển

                        và thông tin liên hệ của người bán). Sau khi tạo tin rao vặt xong người bán sẽ click “

                        <b>Đăng tin này</b>”. Ban quản trị website sẽ có cơ chế kiểm soát những tin bài được up lên.

                        Tất cả những bài vi phạm sẽ bị xóa. Nếu thành viên tái phạm nhiều lần ban quản trị sẽ khóa tài
                        khoản. <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ gửi cho thành viên một
                        mã số để đại diện cho tin

                        đăng đó.
                    </p>

                    <p style="font-style: italic;">
                        b, Đăng tin rao vặt qua ứng dụng trên smartphone
                    </p>

                    <p>
                        Người bán đăng nhập tài khoản thông qua ứng dụng trên smartphone. Trên giao

                        diện của ứng dụng chon “<b>Bán nhanh</b>”, sau đó chụp ảnh sản phẩm hoặc chọn ảnh từ kho

                        ảnh có sẵn trên smartphone. Chọn khu vực gian hàng theo doanh mục có sẵn, đăng tiêu

                        đề bán sản phẩm, giá bán , địa chỉ. Người bán mô tả cụ thể thông tin về sản phẩm (Mô tả

                        các điều khoản bán hàng như: chính sách bảo hành, đổi trả, vận chuyển). Sau khi đã điền

                        đầy đủ thông tin rao vặt người bán sẽ kích vào “<b>Hoàn Tất</b>” để đăng bán sản phẩm.
                    </p>

                    <p style="font-weight: bold;font-size: 15px;">Chính sách giao nhận vận chuyển</p>

                    <p>
                        Người mua toàn quyền thỏa thuận với người bán về phương thức giao hàng

                        với món hàng mình đã đặt mua, có thể giao trực tiếp, có thể gửi qua bưu điện, thuê

                        bên thứ ba chuyển phát tùy thuộc vào 2 bên thỏa thuận với nhau về việc giao nhận.

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ có trách nhiệm cung cấp thông
                        tin cho người mua hoặc người bán

                        khi có tranh chấp trong quá trình giao dịch nếu có phát sinh mâu thuẩn xảy ra sau khi

                        giao hàng.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> yêu cầu người bán khi đăng tin
                        bài rao bán sản phẩm phải đưa

                        đầy đủ thông tin về chính sách vận chuyển, thanh toán.
                    </p>

                    <p>
                        Người mua cần đọc kỹ những chính sách vận chuyển của người bán trong mỗi tin

                        rao vặt. <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> khuyến cáo người mua đọc
                        phần “thông tin phản hồi” ở dưới

                        mỗi chủ đề để biết được mức độ tin cậy của người bán.
                    </p>

                    <p style="font-weight: bold;font-size: 15px;">Chính sách bảo trì, bảo hành</p>

                    <p>
                        Khi tiến hành giao dịch người mua cần trao đổi về chính sách bảo hành hàng hóa

                        của người bán (nếu có). Đồng thời yêu cầu người bán cung cấp giấy bảo hành hàng hóa

                        (nếu có)
                    </p>

                    <p>
                        Người bán có trách nhiệm tiếp nhận bảo hành sản phẩm dịch vụ cho người mua

                        như trong cam kết giấy bảo hành sản phẩm.
                    </p>

                    <p>
                        Người mua luôn giữ giấy bảo hành và có quyền đến tận nơi cung cấp sản phẩm để bảo

                        hành hoặc yêu cầu đến tận nhà bảo trì đối với sản phẩm cố định sử dụng tại nhà.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> khuyến cáo người mua hàng cần
                        kiểm tra các chính sách bảo

                        hành, báo trì đối với hàng hóa có dự định mua. <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        sẽ không chịu trách

                        nhiệm chính trong việc bảo hành bất kỳ sản phẩm nào. <a href="http://mraovat.vn"
                                                                                target="_blank">www.mraovat.vn</a> chỉ
                        hỗ trợ trong

                        khả năng cho phép để sản phẩm của người mua được bảo hành theo chế độ của người bán.
                    </p>


                    <p style="font-weight: bold;font-size: 15px;">Chính sách đổi trả hàng</p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> không phải là người bán sản phẩm,
                        dịch vụ nên việc đổi trả sản

                        phẩm, dịch vụ sẽ được thược hiện theo chính sách của từng người bán .
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> yêu cầu người bán khi đăng tin
                        bài rao bán sản phẩm phải đưa

                        đầy đủ thông tin về chính sách đổi trả hàng.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> khuyến cáo người mua phải đọc kĩ
                        thông tin, hoặc gọi điện trực

                        tiếp cho người mua tìm hiểu về việc đổi trả hàng như thế nào trước khi quyết định mua
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ hỗ trợ người mua trong khả
                        năng cho phép về việc đổi trả

                        hàng hóa
                    </p>

                    <p style="font-weight: bold;font-size: 15px;">Quy trình hỗ trợ giải quyết khiếu nại/ phản ánh</p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> và người bán có trách nhiệm tiếp
                        nhận khiếu nại và hỗ trợ người mua

                        liên quan đến giao dịch tại website <a href="http://mraovat.vn"
                                                               target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>
                        Khi phát sinh tranh chấp, Công ty đề cao giải pháp thương lượng, hòa giải giữa các

                        bên nhằm duy trì sự tin cậy của thành viên vào chất lượng dịch vụ của <a
                                href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>

                        và thực hiện theo các bước sau:
                    </p>

                    <p>
                        • Bước 1:Thành viên người mua khiếu nại về hàng hóa của người bán qua email:

                        <a href="mailto:contact@mraovat.vn" target="_top">contact@mraovat.vn</a> . Thành viên có thể gửi
                        phản ánh tại mục “<b>Liên hệ</b>” đến ban

                        quản trị.
                    </p>

                    <p>
                        • Bước 2: Bộ phận Chăm Sóc Khách Hàng của <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        sẽ tiếp nhận các

                        khiếu nại của thành viên người mua, tùy theo tính chất và mức độ của khiếu nại thì

                        bên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ có những biện pháp cụ thể
                        hộ trợ người mua để giải quyết

                        tranh chấp đó.
                    </p>

                    <p>
                        • Bước 3: Trong trường nằm ngoài khả năng và thẩm quyền của <a href="http://mraovat.vn"
                                                                                       target="_blank">www.mraovat.vn</a>

                        thì ban quản trị sẽ yêu cầu người mua đưa vụ việc này ra cơ quan nhà nước có

                        thẩm quyền giải quyết theo pháp luật
                    </p>

                    <p style="font-weight: bold;font-size: 15px;font-style:italic;">Người mua gửi khiếu nại tại địa chỉ
                        :</p>

                    <p>
                        Công ty cổ phần công nghệ Thanh Vân
                    </p>

                    <p>
                        Địa chỉ: 114 đường số 2, cư xã Đô Thành, Phường 04, Quận 03, Ho Chi Minh
                    </p>

                    <p>
                        Điện thoại:
                    </p>

                    <p>
                        Email: <a href="mailto:contact@mraovat.vn" target="_top">contact@mraovat.vn</a>
                    </p>

                    <p>
                        Khách hàng có thể gửi thông tin khiếu nại trực tiếp qua phần “<b>Liên hệ</b>”. Thông tin của

                        khách hàng sẽ được gửi đến ban quản trị <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                        Trong thời gian sớm nhất

                        có thể ban quản trị <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ có email
                        phải hồi lại ý kiến phản ánh của khách

                        hàng.
                    </p>

                    <p>
                        www.mraovat.vn tôn trọng và nghiêm tục thực hiện các quy định của pháp luật về bảo

                        vệ quyền lợi của người mua (người tiêu dùng). Vì vậy, đề nghị các thành viên đăng tin

                        rao vặt sản phẩm, dịch vụ trên sàn cung cấp đầy đủ, chính xác, trung thực và chi tiết các thông
                        tin liên quan đến sản phẩm, dịch vụ. Mọi hành vi lừa đảo, gian lận trong kinh

                        doanh đều bị lên án và phải chịu hoàn toàn trách nhiệm trước pháp luật.
                    </p>

                    <p>
                        Các bên bao gồm người bán, người mua sẽ phải có vai trò trách nhiệm trong việc tích cực

                        giải quyết vấn đề. Đối với người bán hàng cần có trách nhiệm cung cấp văn bản giấy tờ

                        chứng thực thông tin liên quan đến sự việc đang gây mâu thuẫn cho khách hàng. Đối với

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ có trách cung cấp những thông
                        tin liên quan đến người mua và người

                        bán nếu được người mua hoặc người bán (liên quan đến tranh chấp đó) yêu cầu.
                    </p>

                    <p>
                        Tùy theo mức độ vi phạm của người bán, Ban quản trị sẽ quyết định gõ bỏ toàn bộ tin rao

                        vặt của người bán dó, đồng thời sẽ chấm dứt quyền thành viên.
                    </p>

                    <p>
                        Nếu thông qua hình thức thỏa thuận mà vẫn không thể giải quyết được mâu thuẫn phát

                        sinh từ giao dịch giữa 2 bên người mua, người bán, thì một trong 2 bên người mua và

                        người bán sẽ có quyền nhờ đến cơ quan pháp luật có thẩm quyền can thiệp nhằm đảm

                        bảo lợi ích hợp pháp của các bên nhất là cho khách hàng.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">IV.Quy trình thanh toán</p>

                    <p>
                        Người mua và người bán có thể tham khảo các phương thức thanh toán sau đây và lựa

                        chọn áp dụng phương thức phù hợp:
                    </p>

                    <p>
                        <b>Cách 1</b>: Thanh toán trực tiếp:
                    </p>

                    <p>
                        Bước 1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;
                    </p>

                    <p>
                        Bước 2: Người mua đến địa chỉ bán hàng
                    </p>

                    <p>
                        Bước 3: Người mua thanh toán và nhận hàng;
                    </p>

                    <p>
                        <b>Cách 2</b>: Thanh toán sau (COD – giao hàng và thu tiền tận nơi. Cách này được ban quản
                        trị <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> khuyến khích):
                    </p>

                    <p>
                        Bước 1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;
                    </p>

                    <p>
                        Bước 2: Người mua xác thực đơn hàng (điện thoại, tin nhắn, email);
                    </p>

                    <p>
                        Bước 3: Người bán xác nhận thông tin Người mua;
                    </p>

                    <p>
                        Bước 4: Người bán chuyển hàng;
                    </p>

                    <p>
                        Bước 5: Người mua nhận hàng và thanh toán.
                    </p>

                    <p>
                        <b>Cách 3</b>: Thanh toán online qua thẻ tín dụng, chuyển khoản
                    </p>

                    <p>
                        Bước 1: Người mua tìm hiểu thông tin về sản phẩm, dịch vụ được đăng tin;
                    </p>

                    <p>
                        Bước 2: Người mua xác thực đơn hàng (điện thoại, tin nhắn, email);
                    </p>

                    <p>
                        Bước 3: Người bán xác nhận thông tin Người mua;
                    </p>

                    <p>
                        Bước 4: Ngưởi mua thanh toán;
                    </p>

                    <p>
                        Bước 5: Người bán chuyển hàng;
                    </p>

                    <p>
                        Bước 6: Người mua nhận hàng.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">V. Đảm bảo an toàn giao dịch</p>

                    <p>
                        Ban quản trị đã sử dụng các dịch vụ để bảo vệ thông tin về nội dung tin rao vặt bán hàng

                        trên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>. Để đảm bảo các giao dịch
                        được tiến hành thành công, hạn chế tối

                        đa rủi ro có thể phát sinh.
                    </p>

                    <p>
                        Người bán phải cung cấp thông tin đầy đủ (tên, địa chỉ, số điện thoại, email) trên mỗi tin

                        rao vặt.
                    </p>

                    <p>
                        Người mua không nên đưa thông tin chi tiết về việc thanh toán với bất kỳ ai bằng email

                        hoặc hình thức liên lạc khác, <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        không chịu trách nhiệm về những thiệt hại

                        hay rủi ro thành viên có thể gánh chịu trong việc trao đổi thông tin của người mua viên

                        qua Internet hoặc email.
                    </p>

                    <p>
                        Không nên trả tiền trước cho người bán mà người mua chưa giao dịch lần nào.

                        Người bán tham khảo thông tin về người bán, chất lượng hàng hóa, giao dịch thông qua

                        muc “<b>Thông tin phản hồi</b>”
                    </p>

                    <p>
                        Người mua tuyệt đối không sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác

                        để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. Nghiêm cấm việc phát tán,

                        truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm của hệ

                        thống website. Mọi vi phạm sẽ bị xử lý theo Quy chế và quy định của pháp luật.

                        Mọi thông tin giao dịch được bảo mật, trừ trường hợp buộc phải cung cấp khi Cơ quan

                        pháp luật yêu cầu.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">VI. Bảo vệ thông tin cá nhân khách
                        hàng</p>

                    <p>
                        <b>1</b>. <i>Mục đích và phạm vi thu thập</i>
                    </p>

                    <p>
                        Việc thu thập dữ liệu chủ yếu trên Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                 target="_blank">www.mraovat.vn</a> bao

                        gồm: email, điện thoại, tên đăng nhập, mật khẩu đăng nhập, địa chỉ khách hàng (thành

                        viên). Đây là các thông tin mà <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        cần thành viên cung cấp bắt buộc khi

                        đăng ký sử dụng dịch vụ và để <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        liên hệ xác nhận khi khách hàng đăng

                        ký sử dụng dịch vụ trên website nhằm đảm bảo quyền lợi cho cho người tiêu dùng.
                    </p>

                    <p>
                        Các thành viên sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử

                        dụng dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình. Ngoài ra, thành

                        viên có trách nhiệm thông báo kịp thời cho Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                         target="_blank">www.mraovat.vn</a>
                        về

                        những hành vi sử dụng trái phép, lạm dụng, vi phạm bảo mật, lưu giữ tên đăng ký và mật

                        khẩu của bên thứ ba để có biện pháp giải quyết phù hợp.
                    </p>

                    <p>
                        <b>2</b>. <i>Phạm vi sử dụng thông tin</i>
                    </p>

                    <p>
                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sử dụng thông
                        tin thành viên cung cấp để:
                    </p>

                    <p>
                        - Cung cấp các dịch vụ đến Thành viên;
                    </p>

                    <p>
                        - Gửi các thông báo về các hoạt động trao đổi thông tin giữa thành
                        viên và Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>;
                    </p>

                    <p>
                        - Ngăn ngừa các hoạt động phá hủy tài khoản người dùng của

                        thành viên hoặc các hoạt động giả mạo Thành viên;
                    </p>

                    <p>- Liên lạc và giải quyết với thành viên trong những trường hợp đặc biệt</p>

                    <p>
                        - Trong trường hợp có yêu cầu của pháp luật: Sàn giao dịch

                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> có trách nhiệm hợp tác cung
                        cấp thông tin cá nhân thành viên

                        khi có yêu cầu từ cơ quan tư pháp bao gồm: Viện kiểm sát, tòa án, cơ quan công an điều

                        tra liên quan đến hành vi vi phạm pháp luật nào đó của khách hàng. Ngoài ra, không ai có

                        quyền xâm phạm vào thông tin cá nhân của thành viên.
                    </p>

                    <p>
                        <b>3</b>. <i>Thời gian lưu trữ thông tin</i>
                    </p>

                    <p>
                        - Dữ liệu cá nhân của Thành viên sẽ được lưu trữ cho đến khi có yêu cầu

                        hủy bỏ hoặc tự thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp

                        thông tin cá nhân thành viên sẽ được bảo mật trên máy chủ của <a href="http://mraovat.vn"
                                                                                         target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>
                        <b>3</b>. <i>Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân</i>
                    </p>

                    <p>
                        • Công ty cổ phần công nghệ Thanh Vân
                    </p>

                    <p>
                        • Trụ sở chính: 114 đường số 2, cư xã Đô Thành, Phường 04, Quận 03, Hồ

                        Chí Minh
                    </p>

                    <p>
                        • Email: <a href="mailto:contact@mraovat.vn" target="_top">contact@mraovat.vn</a>
                    </p>

                    <p>
                        <b>5</b>. <i>Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá

                            nhân của mình.</i>
                    </p>

                    <p>
                        - Thành viên có quyền tự kiểm tra, cập nhật, điều chỉnh hoặc hủy bỏ

                        thông tin cá nhân của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin cá

                        nhân hoặc yêu cầu <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> thực hiện việc
                        này.
                    </p>

                    <p>
                        - Thành viên có quyền gửi khiếu nại về việc lộ thông tin các nhân cho

                        bên thứ 3 đến Ban quản trị của Sàn giao dịch thương mại điện tử <a href="http://mraovat.vn"
                                                                                           target="_blank">www.mraovat.vn</a>.
                        Khi

                        tiếp nhận những phản hồi này, <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ
                        xác nhận lại thông tin, phải có trách

                        nhiệm trả lời lý do và hướng dẫn thành viên khôi phục và bảo mật lại thông tin.
                    </p>

                    <p>
                        <b>6</b>. <i>Cam kết bảo mật thông tin cá nhân khách hàng.</i>
                    </p>

                    <p>- Thông tin cá nhân của thành viên trên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        được

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cam kết bảo mật tuyệt đối theo
                        chính sách bảo vệ thông tin cá nhân của

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>. Việc thu thập và sử dụng thông
                        tin của mỗi thành viên chỉ được thực

                        hiện khi có sự đồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác.</p>

                    <p>
                        - Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về

                        thông tin cá nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.
                    </p>

                    <p>
                        - Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất

                        mát dữ liệu cá nhân thành viên, <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        sẽ có trách nhiệm thông báo vụ việc

                        cho cơ quan chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.
                    </p>

                    <p>
                        - Ban quản lý <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> không chịu trách
                        nhiệm cũng như không giải

                        quyết mọi khiếu nại có liên quan đến quyền lợi của thành viên đó nếu xét thấy tất cả

                        thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính xác.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">VII.Bảo vệ quyền lợi của người tiêu
                        dùng</p>

                    <p>
                        Ban quản lý Sàn yêu cầu các cá nhân khi đăng ký là thành viên, phải cung cấp đầy đủ

                        thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email, số chứng minh nhân

                        dân, điện thoại ..., và chịu trách nhiệm về tính pháp lý của những thông tin trên.
                    </p>

                    <p>
                        Người mua có quyền gửi khiếu nại trực tiếp và yêu cầu bồi thường đến người bán có liên

                        quan trong trường hợp sản phẩm, dịch vụ do người bán thực hiện cung cấp không đảm

                        bảo chất lượng như các thông tin đã công bố.
                    </p>

                    <p>
                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> luôn đảm bảo
                        làm trọng tài khi xét thấy tranh

                        chấp giữa người mua và người bán sản phẩm, dịch vụ là chính xác. Đảm bảo quyền lợi

                        hợp pháp cho người tiêu dùng trong các trường hợp liên quan mâu thuẩn giao dịch gây

                        ảnh hưởng đến lợi ích người mua.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">VIII.Quản lý thông tin xấu</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">1. Quy định thành viên:</p>

                    <p>Thành viên sẽ tự chịu trách nhiệm về bảo mật và lưu giữ mọi hoạt động sử dụng dịch vụ

                        dưới tên đăng ký, mật khẩu của mình. Thành viên có trách nhiệm thông báo kịp thời cho

                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> về những hành
                        vi sử dụng trái phép, lạm dụng, vi

                        phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của bên thứ ba để có biện pháp giải quyết

                        phù hợp.</p>

                    <p>Thành viên đăng tin bài rao bán sản phẩm, dịch vụ sai chuyên mục Ban quản trị

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ gửi email thông báo và xóa bài
                        viết đó.</p>

                    <p>Thành viên không sử dụng dịch vụ của Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                  target="_blank">www.mraovat.vn</a> vào

                        những mục đích bất hợp pháp, không hợp lý, lừa đảo, đe doạ, thăm dò thông tin bất hợp

                        pháp, phá hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu hình, truyền tải thông

                        tin của Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> hay sử
                        dụng dịch vụ của mình vào mục

                        đích đầu cơ, lũng đoạn thị trường tạo những đơn đặt hàng, chào hàng giả, kể cả phục vụ

                        cho việc phán đoán nhu cầu thị trường. Trong trường hợp vi phạm thì thành viên phải

                        chịu trách nhiệm về các hành vi của mình trước pháp luật.

                        Thành viên không được thay đổi, chỉnh sửa, gán gép, copy, truyền bá, phân phối, cung

                        cấp và tạo những công cụ tương tự của dịch vụ do Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                               target="_blank">www.mraovat.vn</a>

                        cung cấp cho một bên thứ ba nếu không được sự đồng ý của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> trong bản Quy chế này.</p>

                    <p>Thành viên không được hành động gây mất uy tín của Sàn giao dịch MTĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> dưới mọi hình thức như gây mất
                        đoàn kết giữa các thành viên bằng

                        cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến

                        những thông tin không có lợi cho uy tín của Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                          target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">2. Danh sách sản phẩm cấm giao dịch và
                        giao dịch có điều kiện tại

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.</p>

                    <p>
                        <b>2.1</b>.Danh sách sản phẩm cấm giao dịch:
                    </p>

                    <p>- Rượu từ 15o trở lên;</p>

                    <p>
                        - Thuốc lá điếu, xì gà và các dạng thuốc lá thành phẩm khác;
                    </p>

                    <p>
                        - Các loại pháo nổ, thuốc pháo nổ;
                    </p>

                    <p>
                        - Các chất ma túy;
                    </p>

                    <p>
                        - Thiết bị gây nhiễu thông tin di động tế bào;
                    </p>

                    <p>
                        - Đèn trời;
                    </p>

                    <p>
                        - Vũ khí quân dụng, trang thiết bị, kĩ thuật, khí tài, phương tiện chuyên dùng quân sự,

                        công an, quân trang;
                    </p>

                    <p>
                        - Đồ chơi nguy hiểm, đồ chơi có hại tới giáo dục nhân cách và sức khỏe của trẻ em hoặc

                        tới an ninh, trật tự, văn hóa xã hội ( bao gồm cả các chương trình trò chơi điện tử);
                    </p>

                    <p>
                        - Các sản phẩm văn hóa phản động, đồi trụy, mê tín dị đoan hoặc có hại tới giáo dục

                        thẩm mỹ, nhân cách
                    </p>

                    <p>
                        - Di vật, cổ vật, bảo vật quốc gia thuộc di tích lịch sử văn hóa và danh lam thắng cảnh,

                        thuộc sở hữu toàn dân, sở hữu của các tổ chức chính trị, tổ chức chính trị - xã hội;
                    </p>

                    <p>
                        - Hóa chất độc; Tiền chất;
                    </p>

                    <p>
                        - Thuốc lá điếu, xì gà và các dạng thuốc lá thành phẩm khác nhập lậu.
                    </p>

                    <p>
                        - Thực vật, động vật hoang dã;
                    </p>

                    <p>
                        - Thủy sản cấm khai thác, thủy sản có dư lượng chất độc hại vượt quá giới hạn cho

                        phép, thủy sản có yếu tố độc tự nhiên gây nguy hiểm đến tính mạng con người;
                    </p>

                    <p>
                        - Các loại thuốc chữa bệnh cho người, các loại vắc xin, sinh phẩm y tế, mỹ phẩm, hóa

                        chất và chế phẩm diệt côn trùng, diệt khuẩn trong lĩnh vực gia dụng và y tế chưa được

                        sử dụng tại Việt Nam;
                    </p>

                    <p>
                        - Các loại trang thiết bị y tế chưa được phép sử dụng tại Việt Nam
                    </p>

                    <p>
                        - Các loại mỹ phẩm y tế chưa được công bố với cơ quan có thẩm quyền;
                    </p>

                    <p>
                        - Mua bán các loại xe không có giấy tờ hay mất giấy tờ (Chú ý khi mua bán xe phải có

                        bên thứ 3 chứng giấy tờ)
                    </p>

                    <p>
                        - Mua bán thông tin cá nhân (email, danh sách khách hàng...)
                    </p>

                    <p>
                        <b>2.2</b>.Danh sách sản phẩm giao dịch có điều kiện:
                    </p>

                    <p>Đối với dụng cụ thể thao tập võ, thành viên bán hàng phải là cửa hàng dụng cụ thể thao

                        hay cơ sở sản xuất dụng cụ thể thao.

                        Đối với các mặt hàng giới tính như bao cao su có thể bán như nhà thuốc, riêng sextoy,

                        kích dụng không được bán.</p>

                    <p>Đối với các mặt hàng phần mềm như account, software,... có bản quyền, phải do công

                        ty có giấy phép kinh doanh rõ ràng. Các cá nhân không được bán mặt hàng này.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">Hành vi cấm trong hoạt động đăng tải tin
                        để giới thiệu, quảng cáo sản phẩm, dịch vụ trên <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>- Quảng cáo thiếu thẩm mỹ, trái với truyền thống lịch sử, văn hóa, đạo đức, thuần

                        phong mỹ tục Việt Nam.
                    </p>

                    <p>
                        - Quảng cáo xúc phạm uy tín, danh dự, nhân phẩm của tổ chức, cá nhân.
                    </p>

                    <p>
                        - Quảng cáo có sử dụng hình ảnh, lời nói, chữ viết của cá nhân khi chưa được cá nhân

                        đó đồng ý, trừ trường hợp được pháp luật cho phép.
                    </p>

                    <p>
                        - Quảng cáo không đúng hoặc gây nhầm lẫn về khả năng kinh doanh, khả năng cung

                        cấp sản phẩm, hàng hóa, dịch vụ của tổ chức, cá nhân kinh doanh sản phẩm, hàng hóa,

                        dịch vụ; về số lượng, chất lượng, giá, công dụng, kiểu dáng, bao bì, nhãn hiệu, xuất xứ,

                        chủng loại, phương thức phục vụ, thời hạn bảo hành của sản phẩm, hàng hoá, dịch vụ đã

                        đăng ký hoặc đã được công bố.
                    </p>

                    <p>
                        - Quảng cáo bằng việc sử dụng phương pháp so sánh trực tiếp về giá cả, chất lượng,

                        hiệu quả sử dụng sản phẩm, hàng hóa, dịch vụ của mình với giá cả, chất lượng, hiệu quả

                        sử dụng sản phẩm, hàng hóa, dịch vụ cùng loại của tổ chức, cá nhân khác.
                    </p>

                    <p>
                        - Quảng cáo có nội dung cạnh tranh không lành mạnh theo quy định của pháp luật về

                        cạnh tranh.
                    </p>

                    <p>
                        - Quảng cáo vi phạm pháp luật về sở hữu trí tuệ.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">Cơ chế rà soát, kiểm soát thông tin về
                        sản phẩm/dịch vụ của Ban quản lý Sàn giao dịch TMĐT đối với sản phẩm/dịch vụ đăng tải trên
                        website:</p>

                    <p>
                        Sau khi khách hàng đăng bán sản phẩm, sản phẩm sẽ được hiện thị ngay. Nhưng các

                        thông tin sản phẩm này sẽ được bộ phận biên tập quản lý nội dung <a href="http://mraovat.vn"
                                                                                            target="_blank">www.mraovat.vn</a>

                        kiểm soát và xét duyệt chặt chẽ hàng giờ liên tục hàng ngày.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ toàn quyền loại bỏ các tin
                        đăng rao bán của khách hàng nếu như tin

                        đăng rao bán vi phạm quy chế đăng tin. Các tin đăng không phù hợp với chuyên mục quy

                        định sẽ bị xóa hoặc <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> chuyển sang
                        chuyên mục khác cho là hợp lý.
                    </p>

                    <p>
                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> giữ quyền quyết định về việc lưu
                        giữ hay loại bỏ tin đã đăng trên trang

                        web này mà không cần báo trước.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">IX.Trách nhiệm trong trường hợp phát
                        sinh lỗi kỹ thuật</p>

                    <p>Sàn giao dịch TMĐT www.mraovat.vn cam kết nỗ lực đảm bảo sự an toàn và ổn định

                        của toàn bộ hệ thống kỹ thuật. Tuy nhiên, trong trường hợp xảy ra sự cố do lỗi của

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>, <a href="http://mraovat.vn"
                                                                                           target="_blank">www.mraovat.vn</a>
                        sẽ ngay lập tức áp dụng các biện pháp để đảm bảo

                        quyền lợi cho người mua hàng.

                        Khi thực hiện các giao dịch trên Sàn, bắt buộc các thành viên phải thực hiện đúng theo

                        các quy trình hướng dẫn.</p>

                    <p>Ban quản lý Sàn giao dịch <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cam kết
                        cung cấp chất lượng dịch vụ tốt nhất

                        cho các thành viên tham gia giao dịch. Trường hợp phát sinh lỗi kỹ thuật, lỗi phần mềm

                        hoặc các lỗi khách quan khác dẫn đến Thành viên không thể tham gia giao dịch được

                        thì các Thành viên thông báo cho Ban quản lý Sàn giao dịch TMĐT qua địa chỉ email:

                        <a href="mailto:contact@mraovat.vn" target="_top">contact@mraovat.vn</a> chúng tôi sẽ khắc phục
                        lỗi trong thời gian sớm nhất, tạo điều kiện

                        cho các Thành viên tham gia Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.

                        Tuy nhiên, Ban quản lý Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        sẽ không chịu trách

                        nhiệm giải quyết trong trường hợp thông báo của các thành viên không đến được Ban

                        quản lý, phát sinh từ lỗi kỹ thuật, lỗi đường truyền, phần mềm hoặc các lỗi khác không

                        do Ban quản lý gây ra.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">X.Quyền và nghĩa vụ của Ban quản lý
                        website TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">1.Quyền của Ban quản lý Sàn giao dịch
                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ tiến hành
                        cung cấp các dịch vụ cho những

                        thành viên tham gia sau khi đã hoàn thành các thủ tục và các điều kiện bắt buộc mà Sàn

                        giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> nêu ra.</p>

                    <p>Trong trường hợp có cơ sở để chứng minh thành viên cung cấp thông tin cho Sàn

                        giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> không chính xác,
                        sai lệch, không đầy đủ hoặc vi phạm

                        pháp luật hay thuần phong mỹ tục Việt Nam thì Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                            target="_blank">www.mraovat.vn</a>

                        có quyền từ chối, tạm ngừng hoặc chấm dứt quyền sử dụng dịch vụ của thành viên.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> có thể chấm dứt
                        quyền thành viên và quyền

                        sử dụng một hoặc tất cả các dịch vụ của thành viên và sẽ thông báo cho thành viên trong

                        thời hạn ít nhất là một (01) tháng trong trường hợp thành viên vi phạm các Quy chế của

                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> hoặc có những
                        hành vi ảnh hưởng đến hoạt động

                        kinh doanh trên Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                              target="_blank">www.mraovat.vn</a>.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ xem xét việc
                        chấm dứt quyền sử dụng

                        dịch vụ và quyền thành viên của thành viên nếu thành viên không tham gia hoạt động

                        giao dịch và trao đổi thông tin trên Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                   target="_blank">www.mraovat.vn</a>
                        liên tục

                        trong ba (03) tháng. Nếu muốn tiếp tục trở thành thành viên và được cấp lại quyền sử

                        dụng dịch vụ thì phải đăng ký lại từ đầu theo mẫu và thủ tục của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.</p>

                    <p>
                        Sàn giao dịch TMĐT www.mraovat.vn có thể chấm dứt ngay quyền sử dụng dịch

                        vụ và quyền thành viên của thành viên nếu Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                        target="_blank">www.mraovat.vn</a>
                        phát

                        hiện thành viên đã phá sản, bị kết án hoặc đang trong thời gian thụ án, trong trường hợp

                        thành viên tiếp tục hoạt động có thể gây cho Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                           target="_blank">www.mraovat.vn</a>

                        trách nhiệm pháp lý, có những hoạt động lừa đảo, giả mạo, gây rối loạn thị trường, gây

                        mất đoàn kết đối với các thành viên khác của Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                           target="_blank">www.mraovat.vn</a>,

                        hoạt động vi phạm pháp luật hiện hành của Việt Nam. Trong trường hợp chấm dứt quyền thành viên
                        và quyền sử dụng dịch vụ thì tất cả các chứng nhận, các quyền của thành viên

                        được cấp sẽ mặc nhiên hết giá trị và bị chấm dứt.
                    </p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> giữ bản quyền
                        sử dụng dịch vụ và các nội

                        dung trên Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> theo
                        các quy dịnh pháp luật về bảo hộ

                        sở hữu trí tuệ tại Việt Nam. Tất cả các biểu tượng, nội dung theo các ngôn ngữ khác nhau

                        đều thuộc quyền sở hữu của Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                        Nghiêm cấm mọi

                        hành vi sao chép, sử dụng và phổ biến bất hợp pháp các quyền sở hữu trên.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> giữ quyền được
                        thay đổi bảng, biểu giá dịch

                        vụ và phương thức thanh toán trong thời gian cung cấp dịch vụ cho thành viên theo nhu

                        cầu và điều kiện khả năng của Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        và sẽ báo trước

                        cho thành viên thời hạn là một (01) tháng.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">2.Nghĩa vụ và trách nhiệm của Ban quản
                        lý Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> có trách nhiệm
                        xây dựng, thực hiện “cơ chế

                        kiểm tra, giám sát để đảm bảo việc cung cấp thông tin của người bán trên sàn giao dịch

                        thương mại điện tử được thực hiện chính xác đầy đủ” theo quy định tại Khoản 4 Điều 36

                        Nghị định 52/2013/NĐ-CP.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> chịu trách
                        nhiệm xây dựng Sàn giao dịch bao

                        gồm một số công việc chính như: nghiên cứu, thiết kế, mua sắm các thiết bị phần cứng

                        và phần mềm, kết nối Internet, xây dựng chính sách phục vụ cho hoạt động Sàn giao dịch

                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> trong điều kiện và phạm vi
                        cho phép. Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ tiến hành triển khai và hợp
                        tác với các đối tác trong việc xây dựng

                        hệ thống các dịch vụ, các công cụ tiện ích phục vụ cho việc giao dịch của các thành viên

                        tham gia và người sử dụng trên Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> có trách nhiệm
                        đứng ra làm trung gian trong

                        việc hòa giải nếu sảy ra tranh chấp giữa người mua và người bán. Sàn sẽ tiếp nhận thông

                        tin phản ảnh, khiếu nại từ người mua và kiểm tra nội dung phản ánh,khiếu nại đó.Nếu xét

                        thấy những phản ánh đó là đúng Sàn sẽ yêu cầu người bán phải giải trình về nhưng thông

                        tin đó. Tuy theo mức độ sai phạm Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        sẽ có phương

                        án giải yêu cầu người bán đền bù cho người mua, sẽ khóa tài khoàn thành viên người bán.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> chịu trách
                        nhiệm xây dựng, bổ sung hệ thống

                        các kiến thức, thông tin về: nghiệp vụ ngoại thương, thương mại điện tử, hệ thống văn

                        bản pháp luật thương mại trong nước và quốc tế, thị trường nước ngoài, cũng như các tin

                        tức có liên quan đến hoạt động của Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                 target="_blank">www.mraovat.vn</a>.</p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ tiến hành
                        các hoạt động xúc tiến, quảng

                        bá Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> ra thị
                        trường nước ngoài trong phạm vi và

                        điều kiện cho phép, góp phần mở rộng, kết nối đáp ứng các nhu cầu tìm kiếm bạn hàng

                        và phát triển thị trường nước ngoài của các thành viên tham gia Sàn giao dịch TMĐT <a
                                href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p>Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> sẽ cố gắng đến
                        mức cao nhất trong phạm

                        vi và điều kiện có thể để duy trì hoạt động bình thường của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> và khắc phục các sự cố như: sự cố
                        kỹ thuật về máy móc, lỗi phần mềm,

                        hệ thống đường truyền internet, nhân sự, các biến động xã hội, thiên tai, mất điện, các

                        quyết định của cơ quan nhà nước hay một tổ chức liên quan thứ ba. Tuy nhiên nếu những

                        sự cố trên xẩy ra nằm ngoài khả năng kiểm soát, là những trường hợp bất khả kháng mà

                        gây thiệt hại cho thành viên thì Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        không phải chịu

                        trách nhiệm liên đới.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">XI. Quyền và trách nhiệm thành viên tham
                        gia Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">1. Quyền của Thành viên Sàn giao dịch
                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p>Thành viên sẽ được cấp một tên đăng ký và mật khẩu riêng để được vào sử

                        dụng trong việc quản lý những giao dịch tại <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        thông qua trang

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> hoặc đăng tin rao vặt, đăng tin
                        sản phẩm, đăng tin khuyến mại của

                        mình trên Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.</p>

                    <p>Thành viên sẽ được nhân viên của Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        hướng dẫn

                        sử dụng được các công cụ, các tính năng phục vụ cho việc mua hàng hoặc đăng tin rao

                        vặt và sử dụng các dịch vụ tiện ích trên Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                       target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>Thành viên có quyền đóng góp ý kiến cho Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                     target="_blank">www.mraovat.vn</a>

                        trong quá trình hoạt động. Các kiến nghị được gửi trực tiếp bằng thư, fax hoặc email đến

                        cho Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>
                        Thành viên được quyền bình luận đúng sự thật về sản phẩm, dịch vụ của người bán.
                    </p>

                    <p>
                        Thành viên được quyền gửi phản ánh tại mục “<b>Liên hệ</b>” đến bán quản trị hoặc gửi

                        thông qua địa chỉ, điện thoại, email ở mục quy trình giải quyết phản ánh/khiếu nại.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">2. Nghĩa vụ của Thành viên Sàn giao dịch
                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a></p>

                    <p>Thành viên sẽ tự chịu trách nhiệm về bảo mật và lưu giữ và mọi hoạt động sử dụng

                        dịch vụ dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình.
                    </p>

                    <p>
                        Thành viên có trách nhiệm thông báo kịp thời cho Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> về những hành vi sử dụng trái
                        phép, lạm dụng, vi phạm bảo mật, lưu

                        giữ tên đăng ký và mật khẩu của mình để hai bên cùng hợp tác xử lý.
                    </p>

                    <p>
                        Thành viên cam kết những thông tin cung cấp cho Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> và những thông tin đang tải lên
                        Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>

                        là chính xác.</p>

                    <p>Thành viên tự chịu trách nhiệm về nội dung, hình ảnh của thông tin Doanh nghiệp và

                        các thông tin khác cũng như toàn bộ quá trình giao dịch với các đối tác trên Sàn giao dịch

                        TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p>
                        Thành viên người bán, người mua có trách nhiệm cung cấp thông tin về giao dịch,

                        hàng hóa hộ trợ Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                              target="_blank">www.mraovat.vn</a> trong việc giải quyết
                        tranh chấp

                        phát sinh giữa người mua và người bán diễn ra qua Sàn.
                    </p>

                    <p>
                        Thành viên người bán có trách nhiệm bồi thường thiệt hại cho người mua nếu chứng

                        minh được lỗi đó thuộc về người bán.</p>

                    <p>Thành viên cam kết, đồng ý không sử dụng dịch vụ của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> vào những mục đích bất hợp pháp,
                        không hợp lý, lừa đảo, đe doạ, thăm

                        rò thông tin bất hợp pháp, phá hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu

                        hình, truyền tải thông tin của Sàn giao dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        hay sử dụng dịch vụ

                        của mình vào mục đích đầu cơ, lũng đoạn thị trường tạo những đơn đặt hàng, chào hàng

                        giả, kể cả phục vụ cho việc phán đoán nhu cầu thị trường. Trong trường hợp vi phạm thì

                        thành viên phải chịu trách nhiệm về các hành vi của mình trước pháp luật.</p>

                    <p>Thành viên cam kết không được thay đổi, chỉnh sửa, sao chép, truyền bá, phân

                        phối, cung cấp và tạo những công cụ tương tự của dịch vụ do Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cung cấp cho một bên thứ ba nếu
                        không được sự đồng ý của Sàn giao

                        dịch TMĐT <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cung cấp cho một bên
                        thứ ba nếu không được sự đồng ý của Sàn giao
                        trong Quy định này.</p>

                    <p>
                        Thành viên không được hành động gây mất uy tín của Sàn giao dịch TMĐT

                        <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> dưới mọi hình thức như gây mất
                        đoàn kết giữa các thành viên bằng

                        cách sử dụng tên đăng ký thứ hai, thông qua một bên thứ ba hoặc tuyên truyền, phổ biến

                        những thông tin không có lợi cho uy tín của Sàn giao dịch TMĐT <a href="http://mraovat.vn"
                                                                                          target="_blank">www.mraovat.vn</a>.
                    </p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">XII.Điều khoản áp dụng</p>

                    <p>Quy chế của Sàn giao dịch điện tử <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        chính thức có hiệu lực thi hành

                        kể từ ngày ký Quyết định ban hành kèm theo Quy chế này. Sàn giao dịch điện tử

                        www.mraovat.vn có quyền và có thể thay đổi Quy chế này bằng cách thông báo lên Sàn

                        giao dịch điện tử <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cho các thành
                        viên biết. Quy chế sửa đổi có hiệu lực

                        kể từ ngày Quyết định về việc sửa đổi Quy chế có hiệu lực. Việc thành viên tiếp tục sử

                        dụng dịch vụ sau khi Quy chế sửa đổi được công bố và thực thi đồng nghĩa với việc thành

                        viên đã chấp nhận Quy chế sửa đổi này.
                    </p>

                    <p>
                        Quy chế hoạt động sẽ được <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a> cập
                        nhật bổ sung liên tục mà không cần

                        thông báo trước. Thành viên tham gia <a href="http://mraovat.vn"
                                                                target="_blank">www.mraovat.vn</a> có trách nhiệm tuân
                        theo quy chế

                        hiện hành khi giao dịch trên website.</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">XIII.Điều khoản cam kết</p>

                    <p>Mọi thành viên và đối tác/người bán hàng khi sử dụng <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        làm giao dịch

                        mua bán trực tuyến thì đồng nghĩa việc các bên có liên quan đã chấp thuận tuân theo quy

                        chế này.</p>

                    <p>Mọi thắc mắc của khách hàng xin vui lòng liên hệ với <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                        theo thông tin

                        dưới đây để được giải đáp:</p>

                    <p class="title" style="font-weight: bold;font-size: 15px;">Hỗ trợ khách hàng</p>

                    <p>Sàn giao dịch thương mại điện tử <a href="http://mraovat.vn" target="_blank">www.mraovat.vn</a>
                    </p>

                    <p>Địa chỉ: 114 đường số 2, cư xã Đô Thành, Phường 04, Quận 03, Hồ Chí Minh</p>

                    <p>E-mail: <a href="mailto:contact@mraovat.vn" target="_top">contact@mraovat.vn</a></p>

                    <p style="font-weight: bold;position: absolute;right: 20px;">CÔNG TY (đã ký)</p>
                </div>
            </div>
        </div>
    </div>
@stop
