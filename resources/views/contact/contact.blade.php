@extends('layouts.home')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>

    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="http://mraovat.vn/images/img-mobile.png" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="GỬI ĐẾN CHÚNG TÔI THẮC MẮC CỦA BẠN" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />

@stop
@section('content')
    <div class="container contact-form">
        <h1 CLASS="font-Light text-center">GỬI ĐẾN CHÚNG TÔI THẮC MẮC CỦA BẠN</h1>

        <div class="col-md-12">
            @if (count($errors) > 0)
            <div class="alert alert-danger text-center">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="form-horizontal" action="{{URL::to('lien-he/send')}}" method="post">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="username">Họ tên</label>
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="col-sm-9">
                            <input type="username" name="uname" autofocus="" id="username" required=""
                                   placeholder="Họ tên" class="form-control">

                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="email">Email</label>

                        <div class="col-sm-9">
                            <input type="email" name="email" autofocus="" id="email" required=""
                                   placeholder="Địa chỉ Email" class="form-control">

                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="title">Tiêu đề</label>

                        <div class="col-sm-9">
                            <input type="title" name="title" id="title" autofocus="" required="" placeholder="Tiêu đề"
                                   class="form-control">

                            <p class="help-block"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <textarea class="" name="content" placeholder="Lời nhắn" autofocus="" id="loinhan" required=""
                              style="height: 152px;width: 100%">

                    </textarea>

                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-danger">Gửi đi</button>
                </div>
            </form>
        </div>
    </div>
    <br/>
    <div class="i-map">
        <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d244.96891551575376!2d106.6813136!3d10.7727612!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f3d1edc89dd%3A0xb24a3e2cbae35981!2zdsSDbiBwaMOybmcgNWdpYXkudm4!5e0!3m2!1svi!2s!4v1418786796304"
                    width="100%" height="450" frameborder="0" style="border:0"></iframe>
        </div>
    </div>
    <div class="bg-map"></div>
    <div class="footer-bg"></div>
    @include('includes.social')
@stop

@section('footer')
    {{--<script src="/js/bootstrapValidator.js"></script>--}}
@stop
