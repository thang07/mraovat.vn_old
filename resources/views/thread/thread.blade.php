@extends('layouts.home')
@section('title')
    {{AppHelper::GetCategoryNameUtf($categories,$id)}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>

    <meta property="og:site_name" content="mraovat.vn"/>
    <meta property="og:type" content="article"/>
    @if(count($threads)>0)
    @if($threads[0]->first_post->post_attachment_count>0)
        <meta property="og:image" content="{{$threads[0]->first_post->attachments[0]->links->permalink}}"/>
    @endif
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:title" content="{{$threads[0]->thread_title}}"/>
    <meta property="og:description"
          content="{{$threads[0]->thread_title}},{{$threads[1]->thread_title}},{{$threads[2]->thread_title}},{{$threads[3]->thread_title}},{{$threads[4]->thread_title}},{{$threads[5]->thread_title}}"/>
    @endif
    <meta property="fb:app_id" content="295948790555485"/>

@stop
@section('content')
    @include('includes.search')
    <div class="container grid-pro">
        <div style="margin-bottom: 15px;margin-top: -50px;margin-left: -14px;">
            <div class="row">
                <div class="col-md-12" style="margin-left: 11px; margin-top: -15px;">
                    <h3><i class="glyphicon glyphicon-home"></i> &raquo; <a href="{{URL::to('rao-vat')}}">Rao
                            vặt</a> &raquo; <a
                                href="{{URL::to('rao-vat')}}/{{$url}}">{{AppHelper::GetCategoryNameUtf($categories,$id)}}</a>
                    </h3>
                </div>
                <div class="col-sm-4">
                    <div class="col-md-12">
                        <label for="loc">Khu vực:</label>
                    </div>
                    <div class="col-md-12">
                        <div class="btn-group">
                            <select id="loc" name="location[]" multiple="multiple">
                                @foreach($locations as $loc)
                                    <option value="{{$loc->location_id}}">{{$loc->location_name}}</option>
                                @endforeach
                            </select>
                            <button type="button" id="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-md-12">
                        <label for="from">Giá từ:</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" pattern="^([0-9]){10,}$" id="from" placeholder="0" value="0"
                               class="form-control inputText"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-md-12">
                        <label for="to">Đến:</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" pattern="^([0-9]){10,}$" id="to" value="{{number_format($price)}}"
                               class="form-control inputText"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="col-md-12">
                        <a href="#" id="filter" class="btn btn-primary btnFilter">Xong</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.grid')
    @include('includes.social')
@stop
@section('footer')
    <link href="{{asset('css/bootstrap-multiselect.css')}}" rel="stylesheet">
    <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#loc').multiselect({
                includeSelectAllOption: true,
//            enableFiltering: true,
//            filterPlaceholder:'Tìm kiếm',
                selectAllText: 'Chọn tất cả',
                buttonText: function (options, select) {
                    if (options.length === 0) {
                        return 'Chọn khu vực... <b class="caret"></b>';
                    } else if (options.length > 0) {
                        return options.length + ' khu vực đã chọn <b class="caret"></b>';
                    } else {
                        var labels = [];
                        options.each(function () {
                            if ($(this).attr('label') !== undefined) {
                                labels.push($(this).attr('label'));
                            }
                            else {
                                labels.push($(this).html());
                            }
                        });
                        return labels.join(', ') + ' <b class="caret"> ';
                    }
                }
            });
            $('#reset').on('click', function () {
                $('#loc').multiselect('deselectAll', false);
                $('#loc').multiselect('updateButtonText');
            });
            var loading = false;
            var page = 2;
            var isFilter = false;
            var min = 0;
            var max = 0;
            var location = "";
            $("#filter").click(function () {
                min = $("#from").val().toString().replace(/,/g, "");
                max = $("#to").val().toString().replace(/,/g, "");
                var multipleValues = $("#loc").val() || [];
                location = multipleValues.join(", ");
                isFilter = true;
                page = 2;
                $("#banner0").empty();
                $('.animation_image').show();
                $.ajax({
                    type: "GET",
                    url: "{{URL::to('/filter')}}",
                    data: {'id': '{{$id}}', 'min': min, 'max': max, 'loc': location},
                    cache: false,
                    success: function (data) {
                        $("#banner0").append(data);
                        $('.animation_image').hide();
                    },
                    error: function (xhr, status, error) {
                        //alert(xhr.responseText);
                        $('.animation_image').hide();
                    }
                });
            });

            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 800) {
                    if (loading == false) {
                        loading = true;
                        $('.animation_image').show();
                        $.ajax({
                            type: "GET",
                            url: "{{URL::to('/load-more')}}",
                            data: {
                                'name': '{{$url}}',
                                'id': '{{$id}}',
                                'page': page,
                                'min': min,
                                'max': max,
                                'loc': location,
                                'isFilter': isFilter
                            },
                            cache: false,
                            success: function (data) {
                                $("#banner0").append(data);
                                $('.animation_image').hide();
                                page++;
                                loading = false;
                            },
                            error: function (xhr, status, error) {
                                //alert(xhr.responseText);
                                loading = true;
                                $('.animation_image').hide();
                            }
                        });
                    }
                }
            });
        });
    </script>
@stop
