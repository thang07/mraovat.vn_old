@extends('layouts.home')
@section('title')
    {{$thread->thread_title}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>

    <meta property="og:site_name" content="mraovat.vn"/>
    <meta property="og:type" content="article"/>
    @if($thread->first_post->post_attachment_count >0&&isset($thread->first_post->attachments))
        <meta property="og:image" content="{{$thread->first_post->attachments[0]->links->permalink}}"/>
    @endif
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:title" content="{{$thread->thread_title}}"/>
    <meta property="og:description" content="{{$thread->first_post->post_body}}"/>
    <meta property="fb:app_id" content="295948790555485"/>

@stop
@section('content')
    <div class="round-detail-list">
        @include('includes.search')
        <div class="container details-page">
            <div class="col-md-12" style="margin-left: -12px;">
                <h3 class="h3-detail"><i class="glyphicon glyphicon-home"></i> &raquo; <a href="{{URL::to('rao-vat')}}">Rao
                        vặt</a> &raquo; <a
                            href="{{URL::to('rao-vat')}}/{{AppHelper::GetCategoryUrl($categories,$thread->forum_id)}}">
                        {{AppHelper::GetCategoryNameUtf($categories,$thread->forum_id)}}</a></h3>
            </div>
            <div class="row details-round">
                <div class="col-md-6">
                    <div class="clearfix">
                        <div class="clearfix" style="text-align: center;">
                            @if(isset($thread->first_post->attachments)&&count($thread->first_post->attachments) >0 )
                                <a style="display: inline-block;"
                                   href="{{$thread->first_post->attachments[0]->links->permalink}}" class="jqzoom"
                                   rel='gal1' title="{{$thread->thread_title}}">
                                    <img style="width: 100%;height:100%;margin: 0 auto;"
                                         src="{{$thread->first_post->attachments[0]->links->permalink}}"
                                         title="{{$thread->thread_title}}"
                                         class="img-responsive" alt="img"/>
                                </a>
                            @endif
                        </div>
                        <br/>

                        <div class="clearfix">
                            <ul id="thumblist" class="clearfix">
                                @if(isset($thread->first_post->attachments)&&count($thread->first_post->attachments) >0 )
                                    @foreach ($thread->first_post->attachments as $key=>$item)
                                        <li><a class="zoomThumbActive" href='javascript:void(0);'
                                               rel="{gallery: 'gal1', smallimage: '{{ $item->links->permalink}}',largeimage: '{{ $item->links->permalink}}'}">
                                                <img style="width: 50px;height: 50px;"
                                                     src='{{$item->links->thumbnail}}'
                                                     class="img-responsive" alt="img"></a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media">
                        <a class="pull-left" href="{{$user->links->permalink}}">
                            <img class="media-object img-circle"
                                 style="width: 45px;height: 45px;"
                                 src="{{$user->links->avatar}}"/></a>

                        <div class="media-body">
                            <label class="username"><a class="media-heading"
                                                       href="{{$user->links->permalink}}">{{$user->user_title}}</a></label>
                            <br/>
                            {{$user->followers_count}} Followers | {{$user->followings_count}} Following
                        </div>
                    </div>
                    <div class="hr"></div>
                    <div class="p details-products">
                        <h2 class="title">{{$thread->thread_title}}</h2>

                        <p class="glyphicon-sign">
                            {!!$thread->first_post->post_body_html!!}
                        </p>

                        <p class="price">
                            <span class="glyphicon glyphicon-tag"></span>
                            {{number_format($thread->first_post->post_price)}} vnđ
                        </p>

                        <p class="address">
                            <i class="glyphicon glyphicon-map-marker"></i>
                            Bán tại :
                            <a class="btn-link" target="_blank"
                               href="http://maps.google.com/?q={{$thread->first_post->post_address}}"> {{$thread->first_post->post_address}}
                            </a>
                        </p>

                        <div class="icon icon-groups">
                            <label>
                                <i class="glyphicon glyphicon-heart"></i>
                                {{$thread->first_post->post_like_count}}
                            </label>
                            <label>
                                <i class="glyphicon glyphicon-comment"></i>
                                {{$thread->thread_post_count-1}}
                            </label>
                            <label class="like-face">
                                <div class="fb-like" data-href="{{URL::current()}}" data-width="100px"
                                     data-layout="button_count"
                                     data-action="like" data-show-faces="true" data-share="true"></div>
                                <div id="fb-root"></div>

                            </label>
                        </div>

                        <div class="avatar">
                            @foreach ($comments as $item)
                                <div class="media">
                                    <a class="pull-left" href="{{URL::to($item->poster_username)}}">
                                        <img style="width: 40px;height: 40px;" class="media-object img-circle "
                                             src="{{ $item->links->poster_avatar}}" alt="..."
                                                >
                                    </a>

                                    <div class="media-body">
                                        <a class="pull-left" href="{{URL::to($item->poster_username)}}">
                                            <h5 class="media-heading username">
                                                {{$item->poster_name}}</h5>
                                        </a>
                                        <em class="date-post">{{AppHelper::showDate($item->post_create_date)}}</em>

                                        <p class="content-comments">{{$item->post_body}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if(@Session::has('userInfo'))
                            <div class="form-comnt">
                                <form method="POST" action="{{URL::to('comment')}}" accept-charset="UTF-8">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-group">
                                        <input type="hidden" value="{{$thread->thread_id}}" name="ThreadId">
                                        <input placeholder="Nhập bình luận" class="form-control" name="postBody"
                                               type="text" value="">
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-default btn-submit" type="submit" value="Gửi">
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="round-gird">
            <div class="title container"><h2 class="font-Light">SẢN PHẨM CÓ THỂ BẠN QUAN TÂM</h2></div>
            @include('includes.grid')
        </div>
    </div>
@stop
@section('footer')
    <style type="text/css">
        ul#thumblist {
            display: block;
        }

        ul#thumblist li {
            float: left;
            margin-right: 2px;
            list-style: none;
        }

        ul#thumblist li a {
            display: block;
            border: 1px solid #CCC;
        }

        ul#thumblist li a.zoomThumbActive {
            border: 1px solid red;
        }
    </style>

    <link href="{{asset('css/jquery.jqzoom.css')}}" rel="stylesheet">
    <script src="{{asset('js/jquery-1.5.js')}}"></script>
    <script src="{{asset('js/jquery.jqzoom-core.js')}}"></script>

    <script>
        $('.jqzoom').jqzoom({
            zoomType: 'standard',
            lens: true,
            preloadImages: false,
            alwaysOn: false
        });
        ///like share facebook
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=295948790555485&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@stop