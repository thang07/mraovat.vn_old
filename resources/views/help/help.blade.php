@extends('layouts.home')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
    <meta property="og:site_name" content="mraovat.vn" />
    <meta property="og:image" content="http://mraovat.vn/images/guide-home-img.png" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:title" content="HƯỚNG DẪN SỬ DỤNG MRAOVAT" />
    <meta property="og:description" content="mRaovat là Chợ trực tuyến trên nền điện thoại di động, nơi kinh doanh mua sắm bất kì đồ dùng nào chỉ với vài thao tác đơn giản. mRaovat hỗ trợ tương tác like ,comment, share,... trực tuyến trên gian hàng được hàng triệu người Việt Nam tin dùng " />
    <meta property="fb:app_id" content="295948790555485" />

@stop
@section('content')
    <div class="container">
        <div class="row wrapper-guides">
            <div class="guide-item">
                <div class="col-md-12 text-center">
                    <div class="content">

                            <h1 class="text-center h1 ">HƯỚNG DẪN</h1>

                        <img src="{{URL::to('images/guide-home.png')}}" class="img-responsive guide-home" alt=".."/>
                    </div>
                </div>

                <div class="col-md-4">
                    <img src="{{URL::to('images/guide-home-img.png')}}" class="img-responsive" alt="432x244"
                         data-src="holder.js/244x432" style="width: 244px;height: 432px;display: block" />
                </div>
                <div class="col-md-8">
                    <h3>Trang chủ</h3>

                    <p class="font-Light">- Sản phẩm đa dạng: có nhiều gian hàng như thời trang, nội thất, xe cộ, máy
                        tính, điện thoại, bất động sản, dành cho nàng, dành cho chàng, đồ cổ, …</p>

                    <p class="font-Light">- Hàng hóa được sắp sếp theo từng gian hàng tiện dụng cho người bán và người
                        mua.</p>

                    <p class="font-Light">- Sản phẩm (mới và cũ) được cập nhật liên tục với nhiều mức giá hấp dẫn.</p>
                </div>
            </div>

            <div class="guide-item">
                <div class="col-md-12 text-center">
                    <div class="content">
                        <img src="{{URL::to('images/guide-nearby.png')}}" class="img-responsive guide-home" alt=""/>
                    </div>
                </div>

                <div class="col-md-4">
                    <img src="{{URL::to('images/guide-nearby-img.png')}}" class="img-responsive" alt="432x244"
                         data-src="holder.js/244x432" style="width: 244px;height: 432px;display: block"/>
                </div>
                <div class="col-md-8">
                    <h3>Gần tôi</h3>

                    <p class="font-Light">- Tự động phát hiện vị trí hiện tại của bạn.</p>

                    <p class="font-Light">- Tìm những sản phẩm đang được rao bán xung quanh mình.</p>

                </div>
            </div>
            <div class="guide-item">
                <div class="col-md-12 text-center">
                    <div class="content">
                        <img src="{{URL::to('images/guide-bestsell.png')}}" class="img-responsive guide-home" alt=""/>
                    </div>
                </div>

                <div class="col-md-4">
                    <img src="{{URL::to('images/guide-bestsell-img.png')}}" class="img-responsive media-object"
                         alt="432x244"
                         data-src="holder.js/246x435" style="width: 246px;height: 435px;display: block"/>
                </div>
                <div class="col-md-8">
                    <h3>Bán Nhanh</h3>

                    <p class="font-Light">Chỉ cần bạn Tải ứng dụng mRaovat về điện thoại của mình và thực hiện các bước
                        sau:</p>
                    <ul class="list">

                        <li class="media">
                            <a class="pull-left">
                                <img src="{{URL::to('images/ico-bestsell-1.png')}}" alt="72x72" class="img-responsive"/>
                            </a>

                            <p class="media-body font-Light">- Chụp/Chọn hình sản phẩm cần bán
                            </p>
                        </li>
                        <li class="media">
                            <a class="pull-left">
                                <img src="{{URL::to('images/ico-bestsell-2.png')}}" alt="72x72" class="img-responsive"/>
                            </a>

                            <p class="media-body font-Light">-Chọn danh mục cần bán.
                            </p>
                        </li>
                        <li class="media">
                            <a class="pull-left">
                                <img src="{{URL::to('images/ico-bestsell-3.png')}}" alt="72x72" class="img-responsive"/>
                            </a>

                            <p class="media-body font-Light">-Mô tả về sản phẩm cần bán
                            </p>
                        </li>
                        <li class="media">
                            <a class="pull-left">
                                <img src="{{URL::to('images/ico-bestsell-4.png')}}" alt="72x72" class="img-responsive"/>
                            </a>

                            <p class="media-body font-Light">-Giá cho sản phẩm cần bán
                            </p>
                        </li>
                        <li class="media">
                            <a class="pull-left">
                                <img src="{{URL::to('images/ico-bestsell-5.png')}}" alt="72x72" class="img-responsive"/>
                            </a>

                            <p class="media-body font-Light">-Địa chỉ cho sản phẩm cần bán(có thể chọn trên bản đồ hoặc
                                điền bằng tay)
                            </p>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="guide-item">
                <div class="col-md-12 text-center">
                    <div class="content">
                        <img src="{{URL::to('images/guide-notifi.png')}}" class="img-responsive guide-home" alt=""/>
                    </div>
                </div>

                <div class="col-md-4">
                    <img src="{{URL::to('images/guide-notifi-img.png')}}" class="img-responsive" alt="435x246"
                         data-src="holder.js/246x435" style="width: 246px;height: 435px;display: block"/>
                </div>
                <div class="col-md-8">
                    <h3>Thông báo</h3>

                    <p class="font-Light">- Giúp bạn nhận biết những người đã tương tác với mình: like, comment,
                        share...</p>
                </div>
            </div>
            <div class="guide-item">
                <div class="col-md-12 text-center">
                    <div class="content">
                        <img src="{{URL::to('images/guide-me.png')}}" class="img-responsive guide-home" alt=""/>
                    </div>
                </div>

                <div class="col-md-4">
                    <img src="{{URL::to('images/guide-me-img.png')}}" class="img-responsive" alt="435x246"
                         data-src="holder.js/246x435" style="width: 246px;height: 435px;display: block"/>
                </div>
                <div class="col-md-8">
                    <h3>Tôi</h3>

                    <p class="font-Light">- Đăng kí và quản lý thông tin tài khoản trên mRaovat.</p>
                </div>
            </div>
        </div>
        <div class="row settings">
            <div class="col-md-12 text-center">
                <p class="title">
                    <h class="text-center h1 ">CÀI ĐẶT</h>
                </p>

            </div>
            <div class="col-md-8">
                <img src="{{URL::to('images/guide-setting.png')}}" class="img-responsive " alt=""/>
            </div>
            <div class="col-md-4">
                <ul class="list-setting">
                    <li class="media">
                        <a class="pull-left">
                            <i class="glyphicon glyphicon-ok"></i>
                        </a>

                        <p class="font-Light">-Góp ý,đánh giá ứng dụng</p>
                    </li>
                    <li class="media">
                        <a class="pull-left">
                            <i class="glyphicon glyphicon-ok"></i>
                        </a>

                        <p class="font-Light">- Thông tin ứng dụng</p>
                    </li>
                    <li class="media">
                        <a class="pull-left">
                            <i class="glyphicon glyphicon-ok"></i>
                        </a>

                        <p class="font-Light">- Đăng xuất</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('includes.social')
@stop