@extends('layouts.home')
@section('title')
    {{$name}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>
    <meta property="og:site_name" content="mraovat.vn"/>

    {{--@foreach ($news as $item)--}}
    @if($first->first_post->post_attachment_count >0&&isset($first->first_post->attachments))
        <meta property="og:image" content="{{$first->first_post->attachments[0]->links->permalink}}"/>
    @endif
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:title" content="{{$first->thread_title}}"/>
    <meta property="og:description" content="{{substr($first->first_post->post_body,0,250)}}..."/>
    <meta property="fb:app_id" content="295948790555485"/>
    {{--@endforeach--}}

@stop
@section('content')
    @include('includes.search')
    <div class="container ">
        <div class="row category-news">
            <div class="col-sm-8">
                <div class="one-details-new">
                    <div class="thumbnail">
                        <img alt="100%x277" data-src="holder.js/277x100%"
                             style="max-height: 277px; width: 99.9%; display: block;"
                             src="{{$first->first_post->attachments[0]->links->permalink}}" class="img-responsive"/>

                        <div class="caption">
                            <h4>
                                <a href="{{URL::to('blog')}}/{{AppHelper::RemoveUtf($first->thread_title.'-'.$first->thread_id)}}">{{$first->thread_title}}</a>
                            </h4>

                            <p class="font-Light">{{substr($first->first_post->post_body,0,150)}}...</p>
                        </div>
                    </div>
                </div>

                <div class="list-news">
                    <ul>
                        @foreach ($news as $item)
                            <li class="media">
                                <a href="{{URL::to('blog')}}/{{AppHelper::RemoveUtf($item->thread_title).'-'.$item->thread_id}}"
                                   class="pull-left">
                                    <img alt="103x145" style="height: 103px; width: 145px; display: block;"
                                         class="media-object img-rounded img-responsive" data-src="holder.js/145x103"
                                         src="{{$item->first_post->attachments[0]->links->thumbnail}}"></a>

                                <div class="media-body">
                                    <h4 class="media-heading"><a
                                                href="{{URL::to('blog')}}/{{AppHelper::RemoveUtf($item->thread_title).'-'.$item->thread_id}}">{{$item->thread_title}}</a>
                                    </h4>

                                    <p class="font-Light">
                                        {{substr($item->first_post->post_body,0,150)}}...</p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @include('includes.sticky')
        </div>
    </div>
    @include('includes.social')
@stop