@extends('layouts.home')
@section('title')
    {{$thread->thread_title}}
@stop
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="rao vat mien phi nhanh nhat tren mobile,ho tro da nen tang cho nguoi su dung,ho tro post tin vao 5giay.vn"/>
    <meta name="keywords"
          content="rao vat mobile, rao vat, iphone, android, mua, ban, xe co, may tinh, dien thoai, do co"/>
    <meta name="generator" content="mRaovat"/>

    <meta property="og:site_name" content="mraovat.vn"/>
    @if($thread->first_post->post_attachment_count >0&&isset($thread->first_post->attachments))
        <meta property="og:image" content="{{$thread->first_post->attachments[0]->links->thumbnail}}"/>
    @endif
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:title" content="{{$thread->thread_title}}"/>
    <meta property="og:description" content="{{substr( $thread->first_post->post_body,0,250) }}"/>
    <meta property="fb:app_id" content="295948790555485"/>

@stop
@section('content')
    @include('includes.search')
    <div class="container">
        <div class="row details-news">
            <div class="col-sm-8" style="margin-left:0px;">
                <h3 class="link font-Light">
                    <i class="glyphicon glyphicon-home"></i> &raquo; <a href="{{URL::to('/blog')}}">Blog</a>

                </h3>

                <div class="panel panel-default">
                    <div class="panel-heading font-Light">{{AppHelper::showDate($thread->thread_create_date)}}


                        <div class="fb-like text-right" data-href="{{URL::current()}}" data-width="100px"
                             data-layout="button_count"
                             data-action="like" data-show-faces="true" data-share="true"></div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=295948790555485&version=v2.0";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>

                    </div>
                    <div class="panel-body">
                        <h3 class="title">{{$thread->thread_title}}</h3>
                        {!! $thread->first_post->post_body_html !!}
                    </div>
                </div>
            </div>
            @include('includes.sticky')
        </div>
    </div>
    @include('includes.social')
@stop