<?php

return array(

    'mRaovatIOS'     => array(
        'environment' =>'production',
        'certificate' => storage_path() . '/' .'ck.pem',
        'passPhrase'  =>'123456',
        'service'     =>'apns'
    ),
    'mRaovatIOSDev'     => array(
        'environment' =>'development',
        'certificate' => storage_path() . '/' .'ck_dev.pem',
        'passPhrase'  =>'123456',
        'service'     =>'apns'
    ),
    'mRaovatAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyBmX10cZwANIgkYRInSadbPzokpffR1Zn8',
        'service'     =>'gcm'
    )

);