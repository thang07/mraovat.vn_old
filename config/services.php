<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

    'facebook' => [
        'client_id' => '295948790555485',
        'client_secret' => '26420c5a2eace556a49aa6bbad814aac',
        'scope'  => array('email','read_friendlists','user_online_presence'),
    ],

    'google' => [
        'client_id'     => '292808066700-2t5eebotf7b0pkllkdmhkd2u11p7tp76.apps.googleusercontent.com',
        'client_secret' => 'qnxLxmCNIjLpnVUokUeOQ_9B',
        'scope'         => array('userinfo_email', 'userinfo_profile'),
    ],
];
