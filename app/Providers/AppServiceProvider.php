<?php namespace App\Providers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $json = cURL::get(AppHelper::getAPILink('forums?parent_forum_id=0&order=list'));
        $cate = json_decode($json);
        $arr_cate = $cate->forums;
        View::share('categories', $arr_cate);
        Session::put('categories',$arr_cate);
        $detect = new Agent();
        $isMobile = $detect->isMobile();
        View::share('isMobile', $isMobile);
        $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=1&limit=3'));
        $json = json_decode($html);
        $threads=$json->threads;
        View::share('threads',$threads);
        $json=cURL::get(AppHelper::getAPILink('location'));
        $object=json_decode($json);
        $locations=$object->locations;
        View::share('locations',$locations);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
