<?php namespace App;
/**
 * Created by PhpStorm.
 * User: thangnm
 * Date: 10/13/14
 * Time: 9:25 AM
 */

use Illuminate\Database\Eloquent\Model;

class Device extends Model {
    protected $table = 'xf_bdapi_device';
    public $primaryKey = 'id';
    public $timestamps = false;

    static public function scopeOfOs($query, $type){
        return $query->where('device_os', '=', $type);
    }

    static public function scopeIsNotEmpty($query){
        return $query->where('push_token', '<>', '');
    }
}