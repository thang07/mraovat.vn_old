<?php

use anlutro\cURL\Laravel\cURL;

class AppHelper
{
    public static function getAPILink($path)
    {
        return 'http://api.mraovat.vn/' . $path;
    }

    public static function RemoveUtf($str)
    {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = str_replace(" ", "-", str_replace("&*#39;", "", $str));
        $str = str_replace("/", '', $str);
        $str = str_replace("%", '', $str);
        return strtolower($str);
    }

    public static function SplitCharacter($str)
    {
        $str = explode(" ", $str);
        $string = null;
        if (count($str) > 4) {
            $string = $str[0] . $str[1] . $str[2];
        } else {
            foreach ($str as $item) {
                $string .= $item;
            }
            $string = substr($string, 0, 20) . "...";
        }
        return $string;
    }

    public static function GetCategoryUrl($cate, $id)
    {
        foreach ($cate as $cat) {
            if ($cat->forum_id == $id) {
                return AppHelper::RemoveUtf($cat->forum_title) . '-' . $id;
            }
        }
    }

    public static function GetCategoryNameUtf($cate, $id)
    {
        foreach ($cate as $cat) {
            if ($cat->forum_id == $id) {
                return $cat->forum_title;
            }
        }
    }

    public static function GetCategoryNameUrl($id)
    {
        $last = strrpos($id, '-');
        $name = substr($id, 0, $last);
        return $name;
    }

    public static function showDate($date)
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $now = time();
        $time = $now - $date;
        return date('d/m/Y | H:i:s', $date);
    }
}