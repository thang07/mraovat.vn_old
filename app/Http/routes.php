<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/
//V2
//Route::get('/', 'CategoryController@index');
//Route::get('/v2', 'CategoryController@index2');
//V1
Route::controller('push-notification', 'DeviceController');

Route::get('/', 'CategoryController@index2');

Route::get('/rss','RssController@index');

Route::get('/rss/{id}','RssController@detail');

Route::get('/download','WelcomeController@downloadApp');

Route::get('/trang-chu', 'WelcomeController@index');

Route::get('/rao-vat','CategoryController@index2');

Route::get('/rao-vat/{id}','ThreadController@show');

Route::get('/tim-kiem','ThreadController@search');

Route::get('/load-more-search','ThreadController@loadMoreSearch');

Route::get('/rao-vat/{cate}/{thread_id}','ThreadController@detail');

Route::get('/load-more','ThreadController@add');

Route::get('/filter','ThreadController@index');

Route::post('/comment','ThreadController@doComment');

Route::get('/report','ReportController@redirect');

Route::get('/report/{id}','ReportController@index');

Route::get('/report-data','ReportController@getData');

Route::get('/blog','BlogController@index');

Route::get('/blog/{id}','BlogController@show');

Route::get('/huong-dan','HelpController@index');

Route::get('/lien-he','ContactController@index');

Route::post('/lien-he/send','ContactController@send');

Route::get('/load-more-thread-user','UserController@add');

Route::post('/login','UserController@login');

Route::get('/login/fb','UserController@loginFb');

Route::get('/login/gg','UserController@loginGoogle');

Route::get('/logout','UserController@logout');

Route::post('/register','UserController@register');


Route::get('about-us', function () {
    return view('about');
});

Route::get('policy', function () {
    return view('policy');
});

Route::get('/{id}','UserController@index');

Route::get('t/{forum_id}/{id}', 'ThreadController@redirect');

//Route::get('t/{id}', 'ThreadController@redirect2');

