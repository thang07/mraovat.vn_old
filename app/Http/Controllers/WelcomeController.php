<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class WelcomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function downloadApp()
    {
        $detect = new Agent();
        if ($detect->isMobile()) {
            if ($detect->is('iOS')) {
                return Redirect::to('https://itunes.apple.com/vi/app/mraovat/id861916721?mt=8');
            } elseif ($detect->is('AndroidOS') || Agent::is('Android')) {
                return Redirect::to('https://play.google.com/store/apps/details?id=com.lms.mraovat');
            } else {
                return Redirect::to('http://www.windowsphone.com/en-us/store/app/mraovat/22cef701-cf53-4b46-8186-6f416368e15b');
            }
        } else {
            return Redirect::route('/');
        }
    }
}
