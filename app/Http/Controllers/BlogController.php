<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;

class BlogController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @param $str
     * @return Response
     */

    public function index()
    {
        Session::forget('result');
        $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=41&page=1&limit=9'));
        $news = json_decode($json);
        $arr_news = $news->threads;
        $first = $arr_news[0];
        unset($arr_news[0]);
        $detect = new Agent();
        $isMobile = $detect->isMobile();

        $data = ["news" => $arr_news,
            "first" => $first,
            "isMobile" => $isMobile,
            "url" => 'noi-bat-39'
        ];
        return view('blog.blog', $data)->withName('Blog');
    }

    public function show($id)
    {
        $last = strrpos($id, '-');
        $end_id = substr($id, $last + 1, strlen($id) - $last);
        $html = cURL::get(AppHelper::getAPILink('threads/' . $end_id));
        $json = json_decode($html);
        $thread = $json->thread;
        $data = [
            "thread" => $thread,
            "url" => 'noi-bat-39'
        ];
        return view('blog.detail', $data);
    }
}
