<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use App\Device;
use AppHelper;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Jenssegers\Date\Date;

class DeviceController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */

    public function getIndex()
    {
//        return $this->_queues();
//        for($i=0;$i<100;$i++) {
//            Device::find(1)->replicate()->save();
//        }
        if (Session::has('userInfo')) {
            $user = Session::get('userInfo');
            if ($user->is_mod) {
                $json = cURL::get(AppHelper::getAPILink('forums?parent_forum_id=0&order=list'));
                $json_decode = json_decode($json);
                $categories = $json_decode->forums;

                $count = Device::all()->count();
                $countIOS = Device::OfOs('ios')->get()->count();
                $countAnd = Device::OfOs('android')->get()->count();
                $countWP = Device::OfOs('wp')->get()->count();
                return view('backend.push')
                    ->with('count', $count)
                    ->with('countIOS', $countIOS)
                    ->with('countAnd', $countAnd)
                    ->with('countWP', $countWP)
                    ->with('user', $user)
                    ->with('categories', $categories)
                    ->with('action', false);
            }
            return Redirect::to('/');
        } else
            return Redirect::to('/');
    }

    public function postIndex()
    {
        $msg = Input::get('message');
        $thread_id = Input::get('thread_id');
        $forum_id = Input::get('forum_id');

        if(empty($thread_id))
            $thread_id = 0;

        if(empty($forum_id))
            $forum_id = 0;

        return $this->_queues($msg,intval($thread_id),intval($forum_id));
    }


    public function _queues($msg = null, $threadId, $forumId)
    {
//        $devices = Device::all();
//        $devices = Device::where('user_id',1)->get();
        $devices = Device::where('push_token','<>','')->groupBy('push_token')->get();
//        $list = array();
        $i = 0;
        foreach ($devices as $device) {
            $i++;
            $queue = Queue::push('DeviceController', array('msg' => $msg, 'threadId'=>$threadId, 'forumId'=>$forumId, 'token' => $device->push_token, 'os'=>$device->device_os));
            echo $i.': '.$queue.'<br>';
        }
        $this->writeLog('Total PUSH: '.$i);
        $this->writeLog('Message PUSH: '.$msg);
        $this->writeLog('Info PUSH: threadId:'.$threadId .', forumId:'.$forumId);
        return 'done';
    }

    public function fire($job, $data)
    {
        $os = $data['os'];
        $token = $data['token'];
        $msg = $data['msg'];
        $threadId = $data['threadId'];
        $forumId = $data['forumId'];
        $badge = 0;
        if(empty($threadId))
        	$threadId = null;
        if($os=='ios') {
        	$message = PushNotification::Message($msg,array(
        		'badge'=>1,
                'custom'=> array(
                    'thread_id' => $threadId,
                    'forum_id' => $forumId,
                    'user_id' => 0,
                )
       		));
            PushNotification::app('mRaovatIOS')
                ->to($data['token'])
                ->send($message);
//            if(strlen($token)>1)
//                $this->iosPush($token,$msg,$badge,$threadId,$forumId);
        }
        elseif($os=='wp'){
            if(strlen($token)>1)
                $this->windowPhonePush($token,$msg,$badge,$threadId);
        }
        else{
//            $message = PushNotification::Message($data['msg'], array(
//                'data' => array(
//                    'message' => $data['msg'],
//                    'count'=>0,
//                    'user_id'=>null,
//                    'thread_id'=>$threadId,
//                )
//            ));
//            PushNotification::app('mRaovatAndroid')
//                ->to($data['token'])
//                ->send($message);
            if(strlen($token)>1)
                $this->androidPush($token,$msg,$badge,$threadId);
        }
        $this->writeLog($os. ' - ' . $job->getJobId());
        $job->delete();
    }

    public function writeLog($msg){
        File::append(storage_path('logs') . '/push/PUSH-'.Date::now()->format('d-m-Y').'.log', Date::now() . ' - ' . $msg . PHP_EOL);
    }

    public function iosPush($deviceToken, $message, $badge, $threadId, $forumId, $userId=0)
    {
        // Send it to the server
        $passphrase = '123456';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', storage_path() . '/' . 'ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
// Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.push.apple.com:2195', $err,
//            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//            if (!$fp)
//                exit("Failed to connect: $err $errstr" . PHP_EOL);
//            echo 'Connected to APNS' . PHP_EOL;
// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => $badge,
            'thread_id' => $threadId,
            'forum_id' => $forumId,
            'user_id'=>0
        );
// Encode the payload as JSON
        $payload = json_encode($body);
// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
//        $result = fwrite($fp, $msg, strlen($msg));
//            if (!$result)
//                echo 'Message not delivered' . PHP_EOL;
//            else
//                echo 'Message successfully delivered' . PHP_EOL;
// Close the connection to the server
        fclose($fp);
    }

    public function windowPhonePush($url, $title, $count, $threadId)
    {
        $this->_wp_push_toast($url, $title, '', $threadId, $count);
        $this->_wp_push_tile($url, '', $count);
    }

    public function _wp_push_toast($url, $title, $subtitle, $threadId, $count, $delay = 0, $message_id = NULL)
    {
    	if (empty($threadId)) {
    		$msg = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
            "<wp:Notification xmlns:wp=\"WPNotification\">" .
            "<wp:Toast>" .
            "<wp:Text1>" . htmlspecialchars($title) . "</wp:Text1>" .
            "<wp:Text2>" . htmlspecialchars($subtitle) . "</wp:Text2>" .
            "<wp:Count>" . $count . "</wp:Count>" .
            "</wp:Toast>" .
            "</wp:Notification>";
    	}
    	else
	        $msg = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
	            "<wp:Notification xmlns:wp=\"WPNotification\">" .
	            "<wp:Toast>" .
	            "<wp:Text1>" . htmlspecialchars($title) . "</wp:Text1>" .
	            "<wp:Text2>" . htmlspecialchars($subtitle) . "</wp:Text2>" .
	            "<wp:Param>/ViewDetailPage.xaml?thread_id=" . $threadId . "</wp:Param>" .
	            "<wp:Count>" . $count . "</wp:Count>" .
	            "</wp:Toast>" .
	            "</wp:Notification>";

        return $this->_wp_push($url, 'toast', $delay + 2, $message_id, $msg);
    }

    public function _wp_push_tile($url, $count, $delay = 0, $message_id = NULL)
    {
        $msg = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
            "<wp:Notification xmlns:wp=\"WPNotification\">" .
            "<wp:Tile>" .
            "<wp:BackgroundImage></wp:BackgroundImage>" .
            "<wp:Count>" . $count . "</wp:Count>" .
            "<wp:Title></wp:Title>" .
            "</wp:Tile>" .
            "</wp:Notification>";

        return $this->_wp_push($url, 'token', $delay + 2, $message_id, $msg);
    }

    private function _wp_push($url, $target, $delay, $message_id, $msg)
    {
        $sendedheaders = array(
            'Content-Type: text/xml',
            'Accept: application/*',
            "X-NotificationClass: $delay"
        );
        if ($message_id != NULL)
            $sendedheaders[] = "X-MessageID: $message_id";
        if ($target != NULL)
            $sendedheaders[] = "X-WindowsPhone-Target:$target";


        $req = curl_init();
        curl_setopt($req, CURLOPT_HEADER, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, $sendedheaders);
        curl_setopt($req, CURLOPT_POST, true);
        curl_setopt($req, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($req);
        curl_close($req);

        $result = array();
        foreach (explode("\n", $response) as $line) {
            $tab = explode(":", $line, 2);
            if (count($tab) == 2)
                $result[$tab[0]] = trim($tab[1]);
        }
        return $result;
    }

    public function androidPush($registatoin_ids, $message, $count, $threadId)
    {
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => array($registatoin_ids),
            'data' => array(
            	'message' => $message, 
            	'thread_id' => $threadId, 
            	'count'=>$count),
        );
        $headers = array(
            'Authorization: key=AIzaSyBmX10cZwANIgkYRInSadbPzokpffR1Zn8',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
//            if ($result === FALSE) {
//                die('Curl failed: ' . curl_error($ch));
//            }
        // Close connection
        curl_close($ch);
//            echo $result;
    }

}
