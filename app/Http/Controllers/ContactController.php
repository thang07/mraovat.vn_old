<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

//require_once 'lib/swift_required.php';

class ContactController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('contact.contact')->withName('Liên hệ');
	}

    public function send(){


        $rules = array(
            'email' => 'required',
            'content' => 'required',
        );
        $msg = ['email.required' => 'Email không được trống.',
            'content.required' => 'Bạn vui lòng nhập lời nhắn',
        ];
        $validator = Validator::make(Input::all(), $rules,$msg);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $name = Input::get('uname');
            $emailFrom = Input::get('email');
            $messg = Input::get('content');

            $hostEmailTo = 'contact@mraovat.vn';
            $hostPassword = 'lekima@lms.vn1';

            $pFromName = 'Mraovat - Liên hệ '; //display name
            $pSubjetc = Input::get('title'); //the subjetc
            $pBody = "<html>
            <body>
            <p><h3>THÔNG TIN KHÁCH HÀNG LIÊN HỆ - MRAOVAT.VN</h3></p>
            <p>
                <h4>Họ tên: $name</h4>
                <h4>Tiêu đề: $pSubjetc</h4>
                <h4>Email: $emailFrom</h4>
                <h4>Nội dung liên hệ: </h4>
            </p>
            <p>$messg</p>
            </body>
            </html>"; //body html

                $transport= \Swift_SmtpTransport::newInstance('smtp.zoho.com', 465, "ssl")
                ->setUsername($hostEmailTo)
                ->setPassword($hostPassword);

            $mMailer = \Swift_Mailer::newInstance($transport);
            $mEmail = \Swift_Message::newInstance();
            $mEmail->setSubject($pSubjetc);
            $mEmail->setTo(array($hostEmailTo=>$pFromName,$emailFrom=>$name));
            $mEmail->setFrom(array($hostEmailTo=>$pFromName));
            //$mEmail->setCc(array($emailFrom => $name));


            $mEmail->setBody($pBody, 'text/html'); //body html

            if ($mMailer->send($mEmail) == 0) {
                $msgCusse = "Email chưa gửi được";

            } else {
                $msgCusse = "Thông điệp của bạn được gửi đi, chúng tôi sẽ trả lời trong thời gian sớm nhất ";
            }
        }
        $validator->errors()->add('failed', $msgCusse);
        return Redirect::back()->withErrors($validator);

    }

}
