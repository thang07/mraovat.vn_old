<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite;

class UserController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @param $str
     * @return Response
     */

    public function index($user)
    {
        $page = 1;
        $limit = 9;
        //Get user info
        $html = cURL::get(AppHelper::getAPILink('users/?username=' . $user));
        $json = json_decode($html);
        if (isset($json->errors)) {
            return view('errors.503');
        }
        $user = $json->user;
        //Get threads
        $html = cURL::get(AppHelper::getAPILink('threads/?user_id=') . $user->user_id . '&order=thread_create_date_reverse&page=' . $page . '&limit=' . $limit);
        $json = json_decode($html);
        $threads = $json->threads;
        $data = [
            "user" => $user,
            "threads" => $threads,
            "total" => $json->threads_total
        ];
        return view('user.user', $data)->withName($user->user_title);
    }

    public function add()
    {
        $page = Input::get('page');
        $id = Input::get('id');
        $limit = 9;
        $html = cURL::get(AppHelper::getAPILink('threads/?user_id=') . $id . '&order=thread_create_date_reverse&page=' . $page . '&limit=' . $limit);
        $json = json_decode($html);
        $threads = $json->threads;
        $data = [
            "threads" => $threads,
        ];
        return view('includes.userItem', $data);
    }

    public function logout()
    {
        Session::forget('accessToken');
        Session::forget('userInfo');
        Session::forget('Expire');
        Session::forget('Refresh_Token');
        Session::forget('err');
        Session::forget('followingUsers');
        return Redirect::back();
    }

    public function login()
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        $msg = ['email.required' => 'Email không được trống.',
            'password.required' => 'Mật khẩu không được trống'
        ];
        $validator = Validator::make(Input::all(), $rules, $msg);
        if ($validator->fails()) {
            Session::put('err', 'login');
            return Redirect::back()->withErrors($validator);
        } else {
            if (!Session::has('userInfo')) {
                $data = array('username' => Input::get('email'), 'password' => Input::get('password'), 'client_secret' => 'web123456adcdef', 'client_id' => 'web123456', 'grant_type' => 'password');
                $response = cURL::post(AppHelper::getAPILink('oauth/token'), $data);
                if (intval($response->code) == 200) {
                    $json = json_decode($response->body);
                    Session::put('accessToken', $json->access_token);
                    Session::put('Expire', time() + $json->expires_in);
                    Session::put('Refresh_Token', $json->refresh_token);
                    $response = cURL::get(AppHelper::getAPILink('users/me?oauth_token=') . $json->access_token);
                    $user_data = json_decode($response->body);
                    Session::put('userInfo', $user_data->user);
                    return Redirect::back();
                } else {
                    Session::put('err', 'login');
                    $validator->errors()->add('failed', 'Đăng nhập thất bại');
                    return Redirect::back()->withErrors($validator);
                }
            }
        }
    }

    public function register()
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required',
            'repassword' => 'required|same:password',
        );
        $msg = ['email.required' => 'Email không được trống.',
            'password.required' => 'Mật khẩu không được trống',
            'repassword.required' => 'Nhập lại mật khẩu không được trống',
            'repassword.same' => 'Nhập lại mật khẩu không trùng khớp',
        ];
        $validator = Validator::make(Input::all(), $rules, $msg);
        if ($validator->fails()) {
            Session::put('err', 'register');
            return Redirect::back()->withErrors($validator);
        } else {
            $data = array('email' => Input::get('email'),
                'password' => Input::get('password'),
                'user_phone' => Input::get('phone'),
                'user_location' => Input::get('city'),
                'client_id' => 'web123456',
                'user_title' => Input::get('dispname'));
            $response = cURL::post(AppHelper::getAPILink('users'), $data);
            if (intval($response->code) == 200) {
                if (strpos($response->body, 'errors') == false) {
                    $data = json_decode($response->body);
                    $result = $data->token;
                    Session::put('accessToken', $result->access_token);
                    Session::put('Expire', time() + $result->expires_in);
                    Session::put('Refresh_Token', $result->refresh_token);
                    $response = cURL::get(AppHelper::getAPILink('users/me?oauth_token=') . $result->access_token);
                    $user_data = json_decode($response->body);
                    Session::put('userInfo', $user_data->user);
                    return Redirect::back();
                } else {
                    Session::put('err', 'register');
                    $validator->errors()->add('failed', 'Đăng ký thất bại');
                    return Redirect::back()->withErrors($validator);
                }
            }
        }
    }

    public function loginFb(Request $request)
    {
        // get data from request
        $code = $request->get('code');

        // get fb service
        $fb = \OAuth::consumer('Facebook');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!is_null($code)) {
            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken($code);
            $data = array('fb_token' => $token->getAccessToken(), 'client_id' => 'web123456');
            $response = cURL::post(AppHelper::getAPILink('users/facebook'), $data);
            if (intval($response->code) == 200) {
                $result = json_decode($response->body);
                Session::put('accessToken', $result->token->access_token);
                Session::put('Expire', time() + $result->token->expires_in);
                Session::put('Refresh_Token', $result->token->refresh_token);
                Session::put('userInfo', $result->user);
                return Redirect::back();
            }
        }
    }

    public function loginGoogle(Request $request)
    {
        // get data from request
        $code = $request->get('code');

        // get google service
        $googleService = \OAuth::consumer('Google');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!is_null($code)) {
            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);
            $data = array('gg_token' => $token->getAccessToken(), 'client_id' => 'web123456');
            $response = cURL::post(AppHelper::getAPILink('users/google'), $data);
            if (intval($response->code) == 200) {
                $result = json_decode($response->body);
                Session::put('accessToken', $result->token->access_token);
                Session::put('Expire', time() + $result->token->expires_in);
                Session::put('Refresh_Token', $result->token->refresh_token);
                Session::put('userInfo', $result->user);
                return Redirect::back();
            }
        }
    }
}
