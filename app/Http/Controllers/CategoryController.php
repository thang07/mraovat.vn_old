<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;

class CategoryController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index2()
    {
        Session::forget('result');
        return view('categories.category3')->withName('Rao vặt');
    }

    public function index()
    {
        Session::forget('result');
        Session::forget('key');
        $view = Input::get('view');
        if($view!=null){
            Session::put('view', $view);
        }
        $limit = 20;
        $page = 1;
        $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&page=' . $page . '&limit=' . $limit));
        $json = json_decode($html);
        if ($json != null) {
            $threads = $json->threads;
        }
        Session::put('total',$json->threads_total);
        $data = [
            "threads" => $threads,
            "id" => "",
            "url" => "",
            "isSearch" => 'false'
        ];

        return view('categories.category', $data)->withName('Rao vặt');
    }
}
