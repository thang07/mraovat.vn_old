<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;

class ReportController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @param $str
     * @return Response
     */

    public function index($id)
    {
        if (Session::has('userInfo')) {
            $user = Session::get('userInfo');
            if ($user->is_mod) {
                $thread = array();
                $user = array();
                $start = null;
                $end = null;
                $tick = array();
                switch ($id) {
                    case "day":
                        $mm = date("m") - 1;
                        if ($mm < 10) {
                            $mm = '0' . $mm;
                        }
                        $start = date("Y") . '-' . $mm . '-' . date("d");
                        $end = date("Y") . '-' . date("m") . '-' . date("d");
                        $html = cURL::get(AppHelper::getAPILink('report/daily?start=' . $start . '&end=' . $end));
                        $json = json_decode($html);
                        $plots = $json->plots;
                        $threads = $plots->thread;
                        $users = $plots->user_registration;
                        $dateMapThread = $json->dateMap->thread;
                        $dateMapUser = $json->dateMap->user_registration;

                        foreach ($dateMapThread as $key => $value) {
                            foreach ($threads as $th) {
                                if ($th[0] / 1000 == $key) {
                                    array_push($thread, [intval($key) * 1000, $th[1]]);
                                }
                            }
                        }

                        foreach ($dateMapUser as $key => $value) {
                            foreach ($users as $us) {
                                if ($us[0] / 1000 == $key) {
                                    array_push($user, [intval($key) * 1000, $us[1]]);
                                }
                            }
                        }
                        break;
                    case "week":
                        $mm = date("m") - 3;
                        if ($mm < 10) {
                            $mm = '0' . $mm;
                        }
                        $start =date("Y"). '-' . $mm . '-' . date("d");
                        $end = date("Y") . '-' . date("m") . '-' . date("d");
                        $html = cURL::get(AppHelper::getAPILink('report/weekly?start=' . $start . '&end=' . $end));
                        $json = json_decode($html);
                        $plots = $json->plots;
                        $threads = $plots->thread;
                        $users = $plots->user_registration;
                        $dateMapThread = $json->dateMap->thread;
                        $dateMapUser = $json->dateMap->user_registration;

                        foreach ($dateMapThread as $key => $value) {
                            foreach ($threads as $th) {
                                if ($th[0] == $key) {
                                    array_push($thread, [intval($key), $th[1]]);
                                }
                            }
                        }

                        foreach ($dateMapUser as $key => $value) {
                            foreach ($users as $us) {
                                if ($us[0] == $key) {
                                    array_push($user, [intval($key), $us[1]]);
                                }
                            }
                        }

                        if (count($thread) == count($user)) {
                            foreach ($threads as $key => $value) {
                                array_push($tick, [intval($key), $dateMapThread[$key]]);
                            }
                        }

                        if (count($thread) > count($user)) {
                            foreach ($threads as $key => $value) {
                                array_push($tick, [intval($key), $dateMapThread[$key]]);
                            }
                        }
                        if (count($thread) < count($user)) {
                            foreach ($users as $key => $value) {
                                array_push($tick, [intval($key), $dateMapUser[$key]]);
                            }
                        }
                        break;
                    case "month":
                        $mm = date("m");
                        $start = (date("Y") - 1) . '-' . $mm . '-' . date("d");
                        $end = date("Y") . '-' . date("m") . '-' . date("d");
                        $html = cURL::get(AppHelper::getAPILink('report/monthly?start=' . $start . '&end=' . $end));
                        $json = json_decode($html);
                        $plots = $json->plots;
                        $threads = $plots->thread;
                        $users = $plots->user_registration;
                        $dateMapThread = $json->dateMap->thread;
                        $dateMapUser = $json->dateMap->user_registration;

                        foreach ($dateMapThread as $key => $value) {
                            foreach ($threads as $th) {
                                if ($th[0] == $key) {
                                    array_push($thread, [intval($key), $th[1]]);
                                }
                            }
                        }

                        foreach ($dateMapUser as $key => $value) {
                            foreach ($users as $us) {
                                if ($us[0] == $key) {
                                    array_push($user, [intval($key), $us[1]]);
                                }
                            }
                        }

                        if (count($thread) == count($user)) {
                            foreach ($threads as $key => $value) {
                                array_push($tick, [intval($key), $dateMapThread[$key]]);
                            }
                        }

                        if (count($thread) > count($user)) {
                            foreach ($threads as $key => $value) {
                                array_push($tick, [intval($key), $dateMapThread[$key]]);
                            }
                        }
                        if (count($thread) < count($user)) {
                            foreach ($users as $key => $value) {
                                array_push($tick, [intval($key), $dateMapUser[$key]]);
                            }
                        }
                        break;
                    default:
                        return view('errors.503');
                }
                $data =
                    [
                        "thread" => json_encode($thread),
                        "user" => json_encode($user),
                        "tick" => json_encode($tick),
                        "start" => $start,
                        "end" => $end,
                        "type"=>$id
                    ];
                return view('report.report', $data)->withName('Report');
            }
            return Redirect::to('/');
        } else
            return Redirect::to('/');

    }

    public function getData()
    {
        $start = Input::get("start");
        $end = Input::get("end");
        $type = Input::get("type");
        $option = Input::get("option");
        $tick=array();
        $thread = array();
        $user = array();
        switch ($type) {
            case "day":
                $html = cURL::get(AppHelper::getAPILink('report/daily?fetch='.$option.'&start=' . $start . '&end=' . $end));
                $json = json_decode($html);
                $plots = $json->plots;
                $threads = $plots->thread;
                $users = $plots->user_registration;
                $dateMapThread = $json->dateMap->thread;
                $dateMapUser = $json->dateMap->user_registration;

                $thread = array();
                foreach ($dateMapThread as $key => $value) {
                    foreach ($threads as $th) {
                        if ($th[0] / 1000 == $key) {
                            array_push($thread, [intval($key) * 1000, $th[1]]);
                        }
                    }
                }

                $user = array();
                foreach ($dateMapUser as $key => $value) {
                    foreach ($users as $us) {
                        if ($us[0] / 1000 == $key) {
                            array_push($user, [intval($key) * 1000, $us[1]]);
                        }
                    }
                }

                break;
            case "week":
                $html = cURL::get(AppHelper::getAPILink('report/weekly?fetch='.$option.'&start=' . $start . '&end=' . $end));
                $json = json_decode($html);
                $plots = $json->plots;
                $threads = $plots->thread;
                $users = $plots->user_registration;
                $dateMapThread = $json->dateMap->thread;
                $dateMapUser = $json->dateMap->user_registration;

                foreach ($dateMapThread as $key => $value) {
                    foreach ($threads as $th) {
                        if ($th[0] == $key) {
                            array_push($thread, [intval($key), $th[1]]);
                        }
                    }
                }

                foreach ($dateMapUser as $key => $value) {
                    foreach ($users as $us) {
                        if ($us[0] == $key) {
                            array_push($user, [intval($key), $us[1]]);
                        }
                    }
                }

                if (count($thread) == count($user)) {
                    foreach ($threads as $key => $value) {
                        array_push($tick, [intval($key), $dateMapThread[$key]]);
                    }
                }

                if (count($thread) > count($user)) {
                    foreach ($threads as $key => $value) {
                        array_push($tick, [intval($key), $dateMapThread[$key]]);
                    }
                }
                if (count($thread) < count($user)) {
                    foreach ($users as $key => $value) {
                        array_push($tick, [intval($key), $dateMapUser[$key]]);
                    }
                }
                break;
            case "month":
                $html = cURL::get(AppHelper::getAPILink('report/monthly?fetch='.$option.'&start=' . $start . '&end=' . $end));
                $json = json_decode($html);
                $plots = $json->plots;
                $threads = $plots->thread;
                $users = $plots->user_registration;
                $dateMapThread = $json->dateMap->thread;
                $dateMapUser = $json->dateMap->user_registration;

                foreach ($dateMapThread as $key => $value) {
                    foreach ($threads as $th) {
                        if ($th[0] == $key) {
                            array_push($thread, [intval($key), $th[1]]);
                        }
                    }
                }

                foreach ($dateMapUser as $key => $value) {
                    foreach ($users as $us) {
                        if ($us[0] == $key) {
                            array_push($user, [intval($key), $us[1]]);
                        }
                    }
                }

                if (count($thread) == count($user)) {
                    foreach ($threads as $key => $value) {
                        array_push($tick, [intval($key), $dateMapThread[$key]]);
                    }
                }

                if (count($thread) > count($user)) {
                    foreach ($threads as $key => $value) {
                        array_push($tick, [intval($key), $dateMapThread[$key]]);
                    }
                }
                if (count($thread) < count($user)) {
                    foreach ($users as $key => $value) {
                        array_push($tick, [intval($key), $dateMapUser[$key]]);
                    }
                }
                break;
            default:
                return view('errors.503');
        }
        $data =
            [
                "thread" => $thread,
                "user" => $user,
                "alias"=>$tick
            ];
        return json_encode($data);
    }

    public function redirect(){
        return redirect(URL::to('report/day'));
    }
}
