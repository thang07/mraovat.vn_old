<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use AppHelper;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Roumen\Feed\Facades\Feed;


class RssController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        $json = cURL::get(AppHelper::getAPILink('forums?parent_forum_id=0&order=list'));
        $cate = json_decode($json);
        $rssFeed = '<?xml version="1.0" encoding="UTF-8"?>';
        $rssFeed .= '<rss version="2.0">';
        $rssFeed .= '<channel>';
        $rssFeed .= '<title>Category Rss</title>';
        $rssFeed .= '<link>'.URL::to('rss').'</link>';
        $rssFeed .= '<description>Get all categories</description>';

        foreach ($cate->forums as $cate) {
            $rssFeed .= '<item>';
            $rssFeed .= '<title>' . str_replace('&', '&amp;', $cate->forum_title) . '</title>';
            $rssFeed .= '<description><![CDATA[' . $cate->forum_description. ']]></description>';
            $rssFeed .= '<link>' .URL::to('rss').'/' .AppHelper::RemoveUtf(str_replace('&', '&amp;', $cate->forum_title)) . '-' . $cate->forum_id .'</link>';
            $rssFeed .= '</item>';
        }
        $rssFeed .= '</channel>';
        $rssFeed.= '</rss>';
        return Response::make($rssFeed, '200')->header('Content-Type', 'text/xml');
    }

    public function detail($id)
    {
        $last = strrpos($id, '-');
        $end_id = substr($id, $last + 1, strlen($id) - $last);
        if ($end_id == 39) {
            $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=1&limit=10'));
        } else {
            $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $end_id . '&page=1&limit=10'));
        }
        $json = json_decode($html);
        $threads=array();
        if($json!=null){
            $threads=$json->threads;
        }
        $title = substr($id, 0, $last);
        $rssFeed = '<?xml version="1.0" encoding="UTF-8"?>';
        $rssFeed .= '<rss version="2.0">';
        $rssFeed .= '<channel>';
        $rssFeed .= '<title>'.$title.'</title>';
        $rssFeed .= '<link>'.URL::to('rss').'/'.$id.'</link>';
        $rssFeed .= '<description>Show detail category</description>';
        $description=null;
        foreach($threads as $thread){
            $img = null;
            if ($thread->first_post->post_attachment_count > 0) {
                foreach ($thread->first_post->attachments as $att) {
                    $img .= '&nbsp;&nbsp;<img alt="mRaovat" src="' . $att->links->permalink . '" >';
                }
            } else {
                $img = null;
            }
            $phones = null;
            $phone = null;
            $success = preg_match_all('(0[\s\d,.]{9,12})', $thread->first_post->post_body, $matches);
            if ($success) {
                if (count($matches[0]) > 0) {
                    foreach ($matches[0] as $p) {
                        $phone .= str_replace(",", "", $p) . ',';
                    }
                    $last = strrpos($phone, ',');
                    $str = substr($phone, 0, $last);
                    $phones = '<b>Số điện thoại: ' . str_replace(".", "", $str) . '</b>';
                }
            }
            $address = null;
            if ($thread->first_post->post_address != null) {
                $address = '<b>Địa chỉ: ' . $thread->first_post->post_address . '</b><br><br>';
            }
            $price = null;
            if ($thread->first_post->post_price != null) {
                $price = '<b>Giá: ' . number_format($thread->first_post->post_price) . '</b><br><br>';
            }
            $description=$img . '<br><br>' . $thread->first_post->post_body . '<br><br><b>Tên người bán: ' . $thread->first_post->poster_name . '</b><br><br>' . $address . $price . $phones;
            $utm_source="?utm_source=5giay&utm_medium=link&utm_campaign=5giay_connect";
            $rssFeed .= '<item>';
            $rssFeed .= '<title>' . str_replace('&', '&amp;', $thread->thread_title) . '</title>';
            $rssFeed .= '<description><![CDATA[' . $description.']]></description>';
            $rssFeed .= '<link>' .URL::to('rao-vat').'/'.$title.'/'.AppHelper::RemoveUtf($thread->thread_title).'-'.$thread->thread_id.'</link>';
            $rssFeed .= '</item>';
        }

        $rssFeed .= '</channel>';
        $rssFeed.= '</rss>';

        return Response::make($rssFeed, '200')->header('Content-Type', 'text/xml');
    }
}
