<?php namespace App\Http\Controllers;

use anlutro\cURL\Laravel\cURL;
use App\Http\Requests;
use AppHelper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class ThreadController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $id = Input::get('id');
        $min = Input::get('min');
        $max = Input::get('max');
        $loc = Input::get('loc');
        $limit = 9;
        $html = null;
        $page = 1;
        if ($id == 39) {
            $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=' . $page . '&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc));
        } elseif ($id == 40) {
            $followingUsers = Session::get('followingUsers');
            $users_id = null;
            foreach ($followingUsers as $user) {
                $users_id .= $user->user_id . ",";
            }
            $html = cURL::get(AppHelper::getAPILink('threads?user_id=' . $users_id . '&page=' . $page . '&order=thread_update_date_reverse&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc . '&oauth_token=' . Session::get('accessToken')));
        } else {
            $html = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $id . '&page=' . $page . '&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc));
        }
        $json = json_decode($html);
        if ($json != null) {
            $threads = $json->threads;
        }
        $data = ["threads" => $threads];

        return view('includes.item', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function search()
    {
        //V1
        $key = Input::get('q');
        $id = Input::get('id');
        $json = null;
        if (isset($id)) {
            $json = cURL::get(AppHelper::getAPILink('search/threads?forum_id=' . $id . '&q=' . urlencode($key) . '&limit=1000'));
        } else {
            $json = cURL::get(AppHelper::getAPILink("search/threads?q=" . urlencode($key) . "&limit=1000"));
        }
        $thread = json_decode($json);
        $arr_thread = $thread->threads;
        $threads = array();
        if (count($arr_thread) > 0) {
            $cnt = count($arr_thread);
            if ($cnt == 1000) {
                $cnt = $cnt . "+";
            }
            Session::put('result', 'Có <b>' . $cnt . '</b> kết quả tìm kiếm cho <b>' . $key . '</b>');
            Session::put('threads', $arr_thread);
            $links = array();
            if (count($arr_thread) > 9) {
                for ($i = 0; $i < 9; $i++) {
                    $links[] = ['id' => $arr_thread[$i]->thread_id,
                        'uri' => AppHelper::getAPILink('threads/' . $arr_thread[$i]->thread_id),
                        'method' => 'GET'
                    ];
                }
            } else {
                foreach ($arr_thread as $rs) {
                    $links[] = ['id' => $rs->thread_id,
                        'uri' => AppHelper::getAPILink('threads/' . $rs->thread_id),
                        'method' => 'GET'
                    ];
                }
            }

            $html = cURL::newJsonRequest('post', AppHelper::getAPILink('batch'), $links)
                ->setHeader('content-type', 'application/json')
                ->setHeader('Accept', 'json')
                ->setOptions([CURLOPT_VERBOSE => true])
                ->send();

            $json = json_decode($html);
            $jobs = $json->jobs;

            foreach ($jobs as $job) {
                $threads[] = $job->thread;
            }

        } else {
            Session::put('result', 'Không tìm thấy kết quả nào phù hợp với <b>' . $key . '</b>');
        }
        $data = ['threads' => $threads,
        ];
        return view('search.search', $data)->withName($key);
        //V2
//        $loc = Input::get('location');
//        $cate = Input::get('category');
//        $json = null;
//        if ($cate!="all") {
//            $json = cURL::get(AppHelper::getAPILink('search/threads?forum_id=' . $cate . '&q=' . urlencode($key) . '&limit=1000'));
//        } else {
//            $json = cURL::get(AppHelper::getAPILink("search/threads?q=" . urlencode($key) . "&limit=1000"));
//        }
//        $thread = json_decode($json);
//        $arr_thread = $thread->threads;
//        $threads = array();
//        if (count($arr_thread) > 0) {
//            $cnt = count($arr_thread);
//            Session::put('total',$cnt);
//            if ($cnt == 1000) {
//                $cnt = $cnt . "+";
//            }
//            Session::put('threads', $arr_thread);
//            $links = array();
//            if (count($arr_thread) > 20) {
//                for ($i = 0; $i < 20; $i++) {
//                    $links[] = ['id' => $arr_thread[$i]->thread_id,
//                        'uri' => AppHelper::getAPILink('threads/' . $arr_thread[$i]->thread_id),
//                        'method' => 'GET'
//                    ];
//                }
//            } else {
//                foreach ($arr_thread as $rs) {
//                    $links[] = ['id' => $rs->thread_id,
//                        'uri' => AppHelper::getAPILink('threads/' . $rs->thread_id),
//                        'method' => 'GET'
//                    ];
//                }
//            }
//
//            $html = cURL::newJsonRequest('post', AppHelper::getAPILink('batch'), $links)
//                ->setHeader('content-type', 'application/json')
//                ->setHeader('Accept', 'json')
//                ->setOptions([CURLOPT_VERBOSE => true])
//                ->send();
//
//            $json = json_decode($html);
//            $jobs = $json->jobs;
//
//            foreach ($jobs as $job) {
//                $threads[] = $job->thread;
//            }
//
//        } else {
//            Session::put('result', 'Không tìm thấy kết quả nào phù hợp với <b>' . $key . '</b>');
//        }
//        Session::put('key',$key);
//        $view=Input::get('view');
//        if($view!=null){
//            Session::put('view', $view);
//        }
//        $url=null;
//        if($cate=="all"){
//        }else{
//            $categories=Session::get('categories');
//            $url=AppHelper::GetCategoryUrl($categories,$cate);
//        }
//        $data=[
//            "threads" => $threads,
//            "id"=>$cate,
//            "url" => $url,
//            "isSearch"=>'true'
//        ];
//        return view('categories.category', $data)->withName("Rao vặt");
    }

    public function loadMoreSearch()
    {
        //V1
        $arr_thread = Session::get('threads');
        $page = Input::get('page');
        $limit = 20;
        $threads = array();
        $links = array();
        $total_page = intval(count($arr_thread) / $limit);
        if (count($arr_thread) % $limit != 0) {
            $total_page++;
        }
        if ($page <= $total_page) {
            $var = $limit * $page;
            $init = 0;
            if ($page >= 2) {
                $init = $var - $limit;
            }
            if ($var > count($arr_thread)) {
                $var = count($arr_thread);
            }
            for ($i = $init; $i < $var; $i++) {
                $links[] = ['id' => $arr_thread[$i]->thread_id,
                    'uri' => AppHelper::getAPILink('threads/' . $arr_thread[$i]->thread_id),
                    'method' => 'GET'
                ];
            }
            $rs = cURL::newJsonRequest('post', AppHelper::getAPILink('batch'), $links)
                ->setHeader('content-type', 'application/json')
                ->setHeader('Accept', 'json')
                ->setOptions([CURLOPT_VERBOSE => true])
                ->send();
            $result = json_decode($rs);
            foreach ($result->jobs as $item) {
                $threads[] = $item->thread;
            }
        }

        $data = ['threads' => $threads,
        ];

        return view('includes.item', $data);
        //V2
//        $view=Input::get('view');
//        if($view=='grid'){
//            return view('includes.itemListImg', $data);
//        }else{
//            return view('includes.itemList', $data);
//        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //V1
        Session::forget('result');
        Session::forget('key');
        $json = null;
        $page = 1;
        $limit = 20;
        $arr_thread = array();
        $last = strrpos($id, '-');
        $end_id = substr($id, $last + 1, strlen($id) - $last);
        if (is_numeric($id)) {
            $categories = Session::get('categories');
            $uri = URL::to('rao-vat') . '/' . AppHelper::GetCategoryUrl($categories, $id);
            return redirect($uri);
        }
        if ($end_id == 39) {
            $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=' . $page . '&limit=' . $limit));
        } elseif ($end_id == 40) {
            if (Session::has('accessToken')) {
                $html = cURL::get(AppHelper::getAPILink('users/me/followings?oauth_token=' . Session::get('accessToken')));
                $json = json_decode($html);
                $followingUsers = $json->users;
                Session::put('followingUsers', $followingUsers);
                $users_id = null;
                foreach ($followingUsers as $user) {
                    $users_id .= $user->user_id . ",";
                }
                $json = cURL::get(AppHelper::getAPILink('threads?user_id=' . $users_id . '&page=' . $page . '&order=thread_update_date_reverse&limit=' . $limit . '&oauth_token=' . Session::get('accessToken')));
            }
        } else {
            $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $end_id . '&page=' . $page . '&limit=' . $limit));
        }
        $thread = json_decode($json);
        if ($thread != null) {
            $arr_thread = $thread->threads;
        }

      //  Filter
        $html = cURL::get(AppHelper::getAPILink('filter?forum_id=' . $end_id));
        $json = json_decode($html);
        $filters = $json->results;
        foreach ($filters as $filter) {
            $html = cURL::get(AppHelper::getAPILink('filter?filter_id=' . $filter->filter_id));
            $json = json_decode($html);
            $filterValues = $json->results;
            if ($filter->type_id == 1) {
                $prices = $filterValues[0]->max_price;
            } else {
                $locations = $filterValues;
            }
        }

        $data = ["threads" => $arr_thread,
            "id" => $end_id,
            "price" => $prices,
            "locations" => $locations,
            "url" => $id
        ];

        return view('thread.thread', $data);
        //V2
//        Session::put('total',$thread->threads_total);
//        $view=Input::get('view');
//        if($view!=null){
//            Session::put('view', $view);
//        }
//        $data=[
//            "threads" => $arr_thread,
//            "id"=>$end_id,
//            "url" => $id,
//            "isSearch"=>'false'
//        ];
        //return view('categories.category3',$data )->withName('Rao vặt');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $cate
     * @param $thread_id
     * @return Response
     * @internal param int $id
     */
    public function detail($cate, $thread_id)
    {
        $last = strrpos($thread_id, '-');
        $end_id = substr($thread_id, $last + 1, strlen($thread_id) - $last);
        //Get Thread
        $html = cURL::get(AppHelper::getAPILink('threads/' . $end_id));
        $json = json_decode($html);
        if(isset($json->errors)){
            return view('errors.503');

        }else{
            $thread = $json->thread;
        }


        //Get User
        $html = cURL::get(AppHelper::getAPILink('users/' . $thread->creator_user_id));
        $json = json_decode($html);
        $user = $json->user;
        //Get Comment
        $html = cURL::get(AppHelper::getAPILink('posts?thread_id=') . $thread->thread_id . "&order=natural_reverse");
        $json = json_decode($html);
        $comments = $json->posts;
        array_pop($comments);
        //Sản phẩm liên quan
        $str = explode(" ", $thread->thread_title);
        if (count($str) > 3) {
            $keyword = $str[0] . ' ' . $str[1] . ' ' . $str[2];
        } else {
            $keyword = $thread->thread_title;
        }
        $html = cURL::get(AppHelper::getAPILink('search/threads?q=' . urlencode($keyword) . '&limit=6&forum_id=' . $thread->forum_id));
        $json = json_decode($html);

        if (isset($json->errors)) {
            return view('errors.503');
        }
        $threads = array();
        if ($json == null) {
        } else {
            $result = $json->threads;
            $links = array();
            foreach ($result as $rs) {
                $links[] = ['id' => $rs->thread_id,
                    'uri' => AppHelper::getAPILink('threads/' . $rs->thread_id),
                    'method' => 'GET'
                ];
            }
            //var_dump($links);
            $html = cURL::newJsonRequest('post', AppHelper::getAPILink('batch'), $links)
                ->setHeader('content-type', 'application/json')
                ->setHeader('Accept', 'json')
                ->setOptions([CURLOPT_VERBOSE => true])
                ->send();
            $json = json_decode($html);
            if (!isset($json->errors)) {
                //return view('errors.503');
                $jobs = $json->jobs;
                foreach ($jobs as $job) {
                    $threads[] = $job->thread;
                }
            }

        }
        $data = [
            "thread" => $thread,
            "threads" => $threads,
            "user" => $user,
            "comments" => $comments,
            "url" => $cate . '-' . $thread->forum_id
        ];

        return view('thread.detail', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function add()
    {
        //V1
        $id = Input::get('id');
        $page = Input::get('page');
      $isFilter = Input::get('isFilter');
       $link = Input::get('name');
        $limit = 20;
        $json = null;
        if ($isFilter == "true") {
            $min = Input::get('min');
            $max = Input::get('max');
            $loc = Input::get('loc');
            if ($id == 39) {
                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=' . $page . '&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc));
            } elseif ($id == 40) {
                $followingUsers = Session::get('followingUsers');
                $users_id = null;
                foreach ($followingUsers as $user) {
                    $users_id .= $user->user_id . ",";
                }
                $json = cURL::get(AppHelper::getAPILink('threads?user_id=' . $users_id . '&page=' . $page . '&order=thread_update_date_reverse&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc . '&oauth_token=' . Session::get('accessToken')));
            } else {
                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $id . '&page=' . $page . '&limit=' . $limit . '&min_price=' . $min . '&max_price=' . $max . '&location=' . $loc));
            }
        } else {
            if ($id == 39) {
                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=' . $page . '&limit=' . $limit));
            } elseif ($id == 40) {
                $followingUsers = Session::get('followingUsers');
                $users_id = null;
                foreach ($followingUsers as $user) {
                    $users_id .= $user->user_id . ",";
                }
                $json = cURL::get(AppHelper::getAPILink('threads?user_id=' . $users_id . '&page=' . $page . '&order=thread_update_date_reverse&limit=' . $limit . '&oauth_token=' . Session::get('accessToken')));
            } else {
                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $id . '&page=' . $page . '&limit=' . $limit));
            }
        }

        $thread = json_decode($json);
        if ($thread != null) {
            $arr_thread = $thread->threads;
        }
        $data = ["threads" => $arr_thread,
            "url" => $link
        ];
        return view('includes.item', $data);
        //V2
//        $view=Input::get('view');
//        if($id==""){
//            $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&page=' . $page . '&limit=' . $limit));
//        }else{
//            if ($id == 39) {
//                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&sticky=1&page=' . $page . '&limit=' . $limit));
//            } elseif ($id == 40) {
//                $followingUsers = Session::get('followingUsers');
//                $users_id = null;
//                foreach ($followingUsers as $user) {
//                    $users_id .= $user->user_id . ",";
//                }
//                $json = cURL::get(AppHelper::getAPILink('threads?user_id=' . $users_id . '&page=' . $page . '&order=thread_update_date_reverse&limit=' . $limit . '&oauth_token=' . Session::get('accessToken')));
//            } else {
//                $json = cURL::get(AppHelper::getAPILink('threads?order=thread_update_date_reverse&forum_id=' . $id . '&page=' . $page . '&limit=' . $limit));
//            }
//        }
//        $thread = json_decode($json);
//        if ($thread != null) {
//            $arr_thread = $thread->threads;
//        }
//        $data = ["threads" => $arr_thread];
//        if($view=='grid'){
//
//            return view('includes.itemListImg', $data);
//        }else{
//            return view('includes.itemList', $data);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function doComment()
    {
        $this->checkLoginExpire();
        $data = array('oauth_token' => Session::get('accessToken'), 'thread_id' => Input::get('ThreadId'), 'post_body' => Input::get('postBody'));
        $response = cURL::post(AppHelper::getAPILink('posts'), $data);
        return Redirect::back();
    }

    function checkLoginExpire()
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $now = time();
        $date = Session::get('Expire');
        $time = $now - $date;
        if ($time >= 0) {
            $data = array('refresh_token' => Session::get('Refresh_Token'), 'client_secret' => 'web123456adcdef', 'client_id' => 'web123456', 'grant_type' => 'refresh_token');
            $response = cURL::post(AppHelper::getAPILink('oauth/token'), $data);
            $result = json_decode($response->body);
            Session::put('accessToken', $result->access_token);
            Session::put('Expire', time() + $result->expires_in);
            Session::put('Refresh_Token', $result->refresh_token);
        }
    }

    public function redirect($cate, $thread)
    {
        $categories = Session::get('categories');
        //Get Thread
        $html = cURL::get(AppHelper::getAPILink('threads/' . $thread));
        $json = json_decode($html);
        $thread = $json->thread;
        $url = URL::to('rao-vat') . '/' . AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories, $cate)) . '/' . AppHelper::RemoveUtf($thread->thread_title) . '-' . $thread->thread_id;
        return redirect($url);
    }

    public function redirect2($thread)
    {
        $categories = Session::get('categories');
        //Get Thread
        $html = cURL::get(AppHelper::getAPILink('threads/' . $thread));
        $json = json_decode($html);
        $thread = $json->thread;
        $url = URL::to('rao-vat') . '/' . AppHelper::GetCategoryNameUrl(AppHelper::GetCategoryUrl($categories, $thread->forum_id)) . '/' . AppHelper::RemoveUtf($thread->thread_title) . '-' . $thread->thread_id;
        return redirect($url);
    }

}
